#ifndef GLX_C
#define GLX_C
/*{{{void glXSwapBuffers(Display *dpy, GLXDrawable drawable)*/
#define DO_COPY false
void glXSwapBuffers(Display *dpy, GLXDrawable drawable)
{
	struct drawable_t *d;
	XWindowAttributes attrs;
	XShmSegmentInfo info;
	XImage *img;
	Bool r;
	GC gc;

	ONCE_AND_ENTER;
	TRACE("dpy=0x%x:drawable=0x%x", dpy, drawable);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	XLockDisplay(dpy);
	d = get_synced_drawable_noxlock(dpy, drawable, DO_COPY);
	if (d == 0) {
		LOG("ERROR:unable to get a synced drawable 0x%x from display %p", drawable, dpy);
		goto unlock_x;
	}
	memset(&info, 0, sizeof(info));
	img = XShmCreateImage(dpy, d->visual, d->depth, ZPixmap,
					d->shm.m, &info, d->width, d->height);
	if (img == 0) {
		LOG("ERROR:unable to get a xshm image from drawable 0x%x", drawable);
		goto unlock_x;
	}
	TRACE("XShmCreateImage=%p", img);
	memset(&info, 0, sizeof(info));
	info.shmid = d->shm.id;
	info.shmaddr = d->shm.m;	
	info.readOnly = 0;
	r = XShmAttach(dpy, &info);
	if (!r) {
		LOG("ERROR:drawable=0x%x:unable to attach the xserver to the ipc shared memory segment id=%d", drawable, d->shm.id);
		goto destroy_img;
	}
	gc = XDefaultGC(dpy, d->screen);
	r = XShmPutImage(dpy, drawable, gc, img, 0, 0, 0, 0, d->width,
							d->height, False);
	if (!r)
		LOG("ERROR:drawable=0x%x:unable to put the ximage from the ipc shared memory segment id=%d", drawable, d->shm.id);
	r = XShmDetach(dpy, &info);
	if (!r)
		LOG("ERROR:drawable=0x%x:unable to detach the ximage from the ipc shared memory segment id=%d", drawable, d->shm.id);
	/* our "back buffer" is now undefined */
destroy_img:
	XDestroyImage(img);
unlock_x:
	XUnlockDisplay(dpy);
leave:
	LEAVE;
}
#undef DO_COPY
/*}}}*/
/*{{{*/GLXFBConfig *glXChooseFBConfig(Display *dpy, int screen, const int *attribList, int *nitems)
{
	void *r;

	ONCE_AND_ENTER;
	TRACE("dpy=%p:screen=%d:attrib_list=%p:nelement=%p", dpy, screen, attribList, nitems);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = 0;
		goto leave;		
	}
	if (nitems != 0)
		*nitems = 0;
	r = 0;
leave:
	LEAVE;
	return r;
}/*}}}*/
/*{{{*/XVisualInfo *glXGetVisualFromFBConfig(Display *dpy, GLXFBConfig config)
{
	void *r;

	ONCE_AND_ENTER;
	TRACE("dpy=%p:config=%p", dpy, config);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = 0;
		goto leave;		
	}
	r = 0;
leave:
	LEAVE;
	return 0;
}/*}}}*/
/*{{{*/Display *glXGetCurrentDisplay(void)
{
	Display * r;
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("void");
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = 0;
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		r = 0;
		goto leave;
	}
	TRACE("the current thread has a current context, returning its dpy %p", self_data->ctx->current.dpy);
	r = self_data->ctx->current.dpy;
leave:
	LEAVE;
	return r;
}/*}}}*/
/*{{{*/GLXDrawable glXGetCurrentDrawable(void)
{
	int r;
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("void");
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = None;
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		r = None;
		goto leave;
	}
	TRACE("the current thread has a current context %p, returning its drawable 0x%x", self_data->ctx, self_data->ctx->drawable);
	r = self_data->ctx->drawable;
leave:
	LEAVE;
	return r;
}/*}}}*/
/*{{{*/GLXContext glXGetCurrentContext(void)
{
	GLXContext r;
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("void");
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = 0;
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		r = 0;
		goto leave;	
	}
	TRACE("the calling thread has a current context %p", self_data->ctx);
	r = (GLXContext)self_data->ctx;
leave:
	LEAVE;
	return r;
}/*}}}*/
/*{{{ XVisualInfo *glXChooseVisual(Display *dpy, int screen, int *attribList) */
/*
 * We don't validate the default visual against the client attribList,
 * because the steam client have very reasonable demands.
 * That said, if the screen default visual in not BGRA, we don't know how to
 * draw anything.
 */
XVisualInfo *glXChooseVisual(Display *dpy, int screen, int *attribList)
{
	XVisualInfo *r;
	Visual *default_visual;
	VisualID default_visual_id;
	XVisualInfo template;
	int n;

	ONCE_AND_ENTER;
	TRACE("dpy=%p:screen=%d:attrib_list=%p", dpy, screen, attribList);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = 0;
		goto leave;		
	}
	attribList_parse(attribList);
	XLockDisplay(dpy);
	default_visual = XDefaultVisual(dpy, screen);
	if (default_visual == 0) {
		LOG("ERROR:unable to get the default visual, continuing");
		r = 0;
		goto x_unlock;
	}
	default_visual_id = XVisualIDFromVisual(default_visual);
	TRACE("default visual id=%d", default_visual_id);
	memset(&template, 0, sizeof(template));
	template.visualid = default_visual_id;	
	r = XGetVisualInfo(dpy, VisualIDMask, &template, &n);
	/* should have only 1 visual */
	if (r == 0)
		LOG("ERROR:unable to get the default visual info, continuing");
	else
		printf_visualinfo(__func__, r);
x_unlock:
	XUnlockDisplay(dpy);
leave:
	LEAVE;
	return r;
}/*}}}*/
/*{{{*/GLXContext glXCreateContext(Display *dpy, XVisualInfo *vis, GLXContext shareList, Bool direct)
{
	struct ctx_t *c;

	ONCE_AND_ENTER;
	TRACE("dpy=%p:visi=%p:shareList=%p:direct=%d", dpy, vis, shareList, direct);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		c = 0;
		goto leave;		
	}
	c = calloc(1, sizeof(*c));	
	c->create.dpy = dpy;
	memcpy(&c->create.visinf, vis, sizeof(*vis));
	TRACE("created context is %p with the following visual:", c);
	printf_visualinfo(__func__, vis);

	/* non zero defaults */
	c->color.red = 1.;
	c->color.green = 1.;
	c->color.blue = 1.;
	c->color.alpha = 1.;

	c->drawable = None;
	c->read = None;
leave:
	LEAVE;
	return (GLXContext)c;
}/*}}}*/
/*{{{*/Bool glXQueryExtension(Display *dpy, int *errorb, int *event)
{
	Bool r0;
	Bool r1;
	int major;
	int minor;
	Bool shared_pixmap_supported;

	ONCE_AND_ENTER;
	TRACE("dpy=%p:errorBase=%p:eventBase=%p", dpy, errorb, event);
	/* '0' should tell the client to avoid to use it */
	if (errorb != 0)
		*errorb = 0;
	if (event != 0)
		*event = 0; /* specs: unsused */
	XLockDisplay(dpy);
	r1 = XShmQueryExtension(dpy);
	if (!r1) {
		r0 = False;
		LOG("ERROR:unable to query the xserver MIT-SHM extension");
		goto leave;
	}
	r1 = XShmQueryVersion(dpy, &major, &minor, &shared_pixmap_supported);
	if (!r1) {
		r0 = False;
		LOG("ERROR:xshm:unable to query shared pixmap support");
		goto leave;
	}
	if (!shared_pixmap_supported) {
		r0 = False;
		LOG("ERROR:xshm:shared pixmap unsupported");
		goto leave;
	}
	r0 = True;
	/* we are initialized from here */
	glx_extension_enabled = true;
	LOG("MIT-SHM %d.%d extension enabled", major, minor);
leave:
	XUnlockDisplay(dpy);
	LEAVE;
	return r0;
}/*}}}*/
/*{{{*/Bool glXIsDirect(Display *dpy, GLXContext ctx)
{
	Bool r;

	ONCE_AND_ENTER;
	TRACE("dpy=%p:ctx=%p", dpy, ctx);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = False;
		goto leave;		
	}
	if (ctx == 0) {
		r = False;
		goto leave;
	}
	r = True;
leave:
	LEAVE;
	return r;
}/*}}}*/
/*{{{*/void (*glXGetProcAddress(const GLubyte *procname))(void)
{
	void (*r)(void);

	ONCE_AND_ENTER;
	TRACE("procName=%s", procname);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = 0;
		goto leave;		
	}
	r = 0;
leave:
	LEAVE;
	return r;
}/*}}}*/
/*{{{*/void glXDestroyContext(Display *dpy, GLXContext ctx)
{
	struct ctx_t *c;

	ONCE_AND_ENTER;
	TRACE("dpy=%p:ctx=%p", dpy, ctx);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	c = (struct ctx_t *)ctx;
	if (!c->current.attached) {
		TRACE("freeing context");
		ctx_free(dpy, (struct ctx_t*)ctx);
		goto leave;
	}
	TRACE("tagging context as \"destroyed\"");
	c->destroyed = true;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/Bool glXMakeCurrent(Display *dpy, GLXDrawable drawable, GLXContext ctx)
{
	struct ctx_t *new_ctx;
	Bool r;
	struct thd_data_t *self_data;
	struct ctx_t *prev_ctx;

	ONCE_AND_ENTER;
	new_ctx = (struct ctx_t*)ctx;
	TRACE("dpy=%p:drawable=0x%x:ctx=%p", dpy, drawable, ctx);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = False;
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data != 0)
		prev_ctx = self_data->ctx;
	else
		prev_ctx = 0;
	if (drawable == None) {
		if (new_ctx != 0) {
			TRACE("invalid:None drawable with non zero context");
			r = False;
			goto leave;
		} else { /* new_ctx == 0 */
			self_detach_ctx_with(dpy, self_data);
			/* XXX: flush/finish draw here */
			if (prev_ctx->destroyed) {
				TRACE("previous context %p, which we did detach, is tagged destroyed and is current to no thread, freeing", prev_ctx);
				ctx_free(dpy, prev_ctx);
			} else
				prev_ctx->drawable = None;
			r = True;
			goto leave;
		}
	}
	/* drawable != None */
	if (new_ctx == 0) {
		LOG("ERROR:context is 0 from a not-None drawable");
		r = False;
		goto leave;
	}
	/* drawable != None && new_ctx != 0 */
	if (new_ctx->destroyed) {
		TRACE("new context %p is tagged destroyed then it cannot becomme current to the self thread", new_ctx);
		r = False;
		goto leave;
	}
	if (new_ctx == prev_ctx) { /* only force update the drawable */
		TRACE("context %p is already current to self thread, only updating drawable to 0x%x", new_ctx, drawable);
		goto update_drawable;
	}
	if (new_ctx->current.attached) {
		TRACE("new context %p is already current to thread 0x%x", new_ctx, new_ctx->current.thd);
		r = False;
		goto leave;
	}
	/* drawable != None && new_ctx != 0 && new_ctx != prev_ctx */
	if (prev_ctx != 0) {
		TRACE("previous context %p", prev_ctx);
		self_detach_ctx_with(dpy, self_data);
		/* XXX: flush/finish draw here */
		if (prev_ctx->destroyed) {
			TRACE("previous context %p, which we did detach, is tagged destroyed and is current to no thread, freeing", prev_ctx);
			ctx_free(dpy, prev_ctx);
		} else
			prev_ctx->drawable = None;
	}
	/* drawable != None && new_ctx != 0 && prev_ctx == 0 */
	self_attach_ctx(dpy, new_ctx);
update_drawable:
	new_ctx->drawable = drawable;
	TRACE("current context %p was replaced by context %p with drawable 0x%u", prev_ctx, new_ctx, new_ctx->drawable);
	r = True;
leave:
	if (r == True && new_ctx != 0) {/* for setting the viewport the first time */
		if (!new_ctx->was_already_made_current) {
			TRACE("%p:first time ever attached to a thread:setting the viewport to drawable full size");
			ctx_viewport_set_to_drawable_sz(dpy, new_ctx);
			new_ctx->was_already_made_current = true;
		}
	}
	LEAVE;
	return r;
}/*}}}*/
/*{{{*/Bool glXMakeContextCurrent(Display *dpy, GLXDrawable drawable, GLXDrawable read, GLXContext ctx)
{
	/* deny this code path for now */
	return False;
}/*}}}*/
#endif
