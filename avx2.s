; vim: set filetype=fasm foldmethod=marker commentstring=;%s colorcolumn=101 :
include 'x64.inc'
include 'format/elf32.inc'
include 'ext/avx2.inc'
use32
section '.text' executable writeable align 64
;***************************************************************************************************
public glTexXImage2D_avx2
align 16
glTexXImage2D_avx2: namespace glTexXImage2D_avx2
;---------------------------------------------------------------------------------------------------
; input 
dst			:= 0
dst.x_offset		:= 4
dst.y_offset		:= 8
dst.width.pixs_n	:= 12 ; the real width
dst.line.pixs_n		:= 16
src			:= 20
src.line.pixs_n		:= 24 ; the src width is the dst.width
height.lines_n		:= 28
;---------------------------------------------------------------------------------------------------
; stack
;---- frame
define dst.adjust	esp + 4 * -6
define src.adjust	esp + 4 * -5
define ebx_save		esp + 4 * -4
define edi_save		esp + 4 * -3
define esi_save		esp + 4 * -2
define ebp_save		esp + 4 * -1
;--- caller
define ret_addr		esp + 4 * 0
define input		esp + 4 * 1
;---------------------------------------------------------------------------------------------------
	mov [ebx_save], ebx
	mov [edi_save], edi
	mov [esi_save], esi
	mov [ebp_save], ebp
	; input (right after dst adjust, src adjust, ebx, edi, esi, ebp, "return addr")
	mov ebx, dword [input] 
	mov edi, dword [ebx + dst]
	mov esi, dword [ebx + src]
	; dst --------------------------------------------------------------------------------------
	mov eax, dword [ebx + dst.line.pixs_n]
	shl eax, 2
	mov ecx, eax ; = dst.line.bytes_n
	mul dword [ebx + dst.y_offset] ; edx:eax = offset of the line of the first dst rect width
	add edi, eax ; edi = points on the first pix of the line of the first dst rect width
	mov eax, dword [ebx + dst.x_offset]
	shl eax, 2 ; = dst.x_offset.bytes_n
	add edi, eax ; edi = points on the first pix of the dst rect
	; adjust value from the end pix (past the last pix) of a dst rect width to the first pix of the dst rect width on next line
	mov ebp, [ebx + dst.width.pixs_n]
	shl ebp, 2 ; = dst.width.bytes_n
	sub ecx, ebp ; = dst.line.bytes_n - dst.width.bytes_n = adjust value
	mov dword [dst.adjust], ecx
	; src --------------------------------------------------------------------------------------	
	mov edx, dword [ebx + src.line.pixs_n]
	shl edx, 2 ; = src.line.bytes_n
	; adjust value from the end pix (past the last pix) of a src rect width to the first pix of the src rect width on next line
	sub edx, ebp ; = src.line.bytes_n - dst.width.bytes_n = adjust value
	mov dword [src.adjust], edx
	; ------------------------------------------------------------------------------------------
	mov ebx, [ebx + height.lines_n]
align_nops
next_line:
	mov ecx, ebp ; = dst.width.bytes_n
	cmp ecx, 32 * 8
		jb cpy_32_7
; aggressive unrolled line cpy (useless from AMD zen3 with optimized REP instructions)
align_nops
cpy_32_8:	
	vmovdqu ymm0,	yword [esi + 32 * 0]
	vmovdqu ymm1,	yword [esi + 32 * 1]
	vmovdqu ymm2,	yword [esi + 32 * 2]
	vmovdqu ymm3,	yword [esi + 32 * 3]
	vmovdqu ymm4,	yword [esi + 32 * 4] 
	vmovdqu ymm5,	yword [esi + 32 * 5]
	vmovdqu ymm6,	yword [esi + 32 * 6]
	vmovdqu ymm7,	yword [esi + 32 * 7]

	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1
	vmovdqu yword [edi + 32 * 2],	ymm2
	vmovdqu yword [edi + 32 * 3],	ymm3
	vmovdqu yword [edi + 32 * 4],	ymm4
	vmovdqu yword [edi + 32 * 5],	ymm5
	vmovdqu yword [edi + 32 * 6],	ymm6
	vmovdqu yword [edi + 32 * 7],	ymm7

	sub ecx, 32 * 8
	add edi, 32 * 8
	add esi, 32 * 8
	cmp ecx, 32 * 8
		jae cpy_32_8
align_nops
cpy_32_7:
	cmp ecx, 32 * 7
		jb cpy_32_6
	vmovdqu ymm0,	yword [esi + 32 * 0]
	vmovdqu ymm1,	yword [esi + 32 * 1]
	vmovdqu ymm2,	yword [esi + 32 * 2]
	vmovdqu ymm3,	yword [esi + 32 * 3]
	vmovdqu ymm4,	yword [esi + 32 * 4] 
	vmovdqu ymm5,	yword [esi + 32 * 5]
	vmovdqu ymm6,	yword [esi + 32 * 6]

	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1
	vmovdqu yword [edi + 32 * 2],	ymm2
	vmovdqu yword [edi + 32 * 3],	ymm3
	vmovdqu yword [edi + 32 * 4],	ymm4
	vmovdqu yword [edi + 32 * 5],	ymm5
	vmovdqu yword [edi + 32 * 6],	ymm6

	sub ecx, 32 * 7
	add edi, 32 * 7
	add esi, 32 * 7
align_nops
cpy_32_6:
	cmp ecx, 32 * 6
		jb cpy_32_5
	vmovdqu ymm0,	yword [esi + 32 * 0]
	vmovdqu ymm1,	yword [esi + 32 * 1]
	vmovdqu ymm2,	yword [esi + 32 * 2]
	vmovdqu ymm3,	yword [esi + 32 * 3]
	vmovdqu ymm4,	yword [esi + 32 * 4] 
	vmovdqu ymm5,	yword [esi + 32 * 5]

	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1
	vmovdqu yword [edi + 32 * 2],	ymm2
	vmovdqu yword [edi + 32 * 3],	ymm3
	vmovdqu yword [edi + 32 * 4],	ymm4
	vmovdqu yword [edi + 32 * 5],	ymm5

	sub ecx, 32 * 6
	add edi, 32 * 6
	add esi, 32 * 6
align_nops
cpy_32_5:
	cmp ecx, 32 * 5
		jb cpy_32_4
	vmovdqu ymm0,	yword [esi + 32 * 0]
	vmovdqu ymm1,	yword [esi + 32 * 1]
	vmovdqu ymm2,	yword [esi + 32 * 2]
	vmovdqu ymm3,	yword [esi + 32 * 3]
	vmovdqu ymm4,	yword [esi + 32 * 4] 

	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1
	vmovdqu yword [edi + 32 * 2],	ymm2
	vmovdqu yword [edi + 32 * 3],	ymm3
	vmovdqu yword [edi + 32 * 4],	ymm4

	sub ecx, 32 * 5
	add edi, 32 * 5
	add esi, 32 * 5
align_nops
cpy_32_4:
	cmp ecx, 32 * 4
		jb cpy_32_3
	vmovdqu ymm0,	yword [esi + 32 * 0]
	vmovdqu ymm1,	yword [esi + 32 * 1]
	vmovdqu ymm2,	yword [esi + 32 * 2]
	vmovdqu ymm3,	yword [esi + 32 * 3]

	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1
	vmovdqu yword [edi + 32 * 2],	ymm2
	vmovdqu yword [edi + 32 * 3],	ymm3

	sub ecx, 32 * 4
	add edi, 32 * 4
	add esi, 32 * 4
align_nops
cpy_32_3:
	cmp ecx, 32 * 3
		jb cpy_32_2
	vmovdqu ymm0,	yword [esi + 32 * 0]
	vmovdqu ymm1,	yword [esi + 32 * 1]
	vmovdqu ymm2,	yword [esi + 32 * 2]

	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1
	vmovdqu yword [edi + 32 * 2],	ymm2

	sub ecx, 32 * 3
	add edi, 32 * 3
	add esi, 32 * 3
align_nops
cpy_32_2:
	cmp ecx, 32 * 2
		jb cpy_32
	vmovdqu ymm0,	yword [esi + 32 * 0]
	vmovdqu ymm1,	yword [esi + 32 * 1]

	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1

	sub ecx, 32 * 2
	add edi, 32 * 2
	add esi, 32 * 2
align_nops
cpy_32:
	cmp ecx, 32
		jb cpy_16
	vmovdqu ymm0,	yword [esi + 32 * 0]
	vmovdqu yword [edi + 32 * 0],	ymm0

	sub ecx, 32
	add edi, 32
	add esi, 32
align_nops
cpy_16:
	cmp ecx, 16
		jb cpy_8
	vmovdqu xmm0, xword [esi]
	vmovdqu xword [edi], xmm0

	sub ecx, 16
	add edi, 16
	add esi, 16
align_nops
cpy_8:
	cmp ecx, 8
		jb cpy_4
	mov eax, dword [esi + 4 * 0]
	mov edx, dword [esi + 4 * 1]
	mov dword [edi + 4 * 0], eax
	mov dword [edi + 4 * 1], edx

	sub ecx, 8
	add edi, 8
	add esi, 8
align_nops
cpy_4:
	cmp ecx, 4
		jb cpy_2
	mov eax, dword [esi]
	mov dword [edi], eax

	sub ecx, 4
	add edi, 4
	add esi, 4
align_nops
cpy_2:
	cmp ecx, 2
		jb cpy_1
	mov ax, word [esi]
	mov word [edi], ax

	sub ecx, 2
	add edi, 2
	add esi, 2
align_nops
cpy_1:
	test ecx, ecx
		jz prepare_next_line
	mov al,	byte [esi]
	mov byte [edi],	al

	inc edi
	inc esi
align_nops
prepare_next_line:
	dec ebx ; height--
		jz epilog
	add edi, dword [dst.adjust]
	add esi, dword [src.adjust]
	jmp next_line
align_nops
epilog:
	mov ebx, [ebx_save]
	mov edi, [edi_save]
	mov esi, [esi_save]
	mov ebp, [ebp_save]
	vzeroupper ; end of AVX2 code
	ret
end namespace ; glTexXImage2D_avx2
;***************************************************************************************************
public clearcolor_avx2
align 16
clearcolor_avx2: namespace clearcolor_avx2
;---------------------------------------------------------------------------------------------------
; input 
dst			:= 0
dst.width.pixs_n	:= 4
dst.line.bytes_n	:= 8
dst.height.lines_n	:= 12
;---------------------------------------------------------------------------------------------------
; stack
;---- frame
define ebx_save		esp + 4 * -4
define edi_save		esp + 4 * -3
define esi_save		esp + 4 * -2
define ebp_save		esp + 4 * -1
;--- caller
define ret_addr		esp + 4 * 0
define input		esp + 4 * 1
;---------------------------------------------------------------------------------------------------
	mov eax, 0xff252525
	vmovd xmm0, eax
	vpbroadcastd ymm0, xmm0
	vmovdqu ymm1, ymm0
	vmovdqu ymm2, ymm0
	vmovdqu ymm3, ymm0
	vmovdqu ymm4, ymm0
	vmovdqu ymm5, ymm0
	vmovdqu ymm6, ymm0
	vmovdqu ymm7, ymm0

	mov [ebx_save], ebx
	mov [edi_save], edi
	mov [esi_save], esi
	mov [ebp_save], ebp
	; input (right after ebx, edi, esi, ebp, "return addr")
	mov ebx, dword [input] 
	mov edi, dword [ebx + dst]
	; dst --------------------------------------------------------------------------------------
	mov ebp, [ebx + dst.width.pixs_n]
	mov esi, [ebx + dst.line.bytes_n]
	shl ebp, 2 ;  = dst.width.bytes_n
	; adjust value from the end pix (past the last pix) of a dst rect width to the first pix of the dst rect width on next line
	sub esi, ebp ; = dst.line.bytes_n - dst.width.bytes_n = adjust value
	; ------------------------------------------------------------------------------------------
	mov ebx, [ebx + dst.height.lines_n]
align_nops
next_line:
	mov ecx, ebp ; = dst.width.bytes_n
	cmp ecx, 32 * 8
		jb cpy_32_7
; aggressive unrolled line cpy (useless from AMD zen3 with optimized REP instructions)
align_nops
cpy_32_8:	
	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1
	vmovdqu yword [edi + 32 * 2],	ymm2
	vmovdqu yword [edi + 32 * 3],	ymm3
	vmovdqu yword [edi + 32 * 4],	ymm4
	vmovdqu yword [edi + 32 * 5],	ymm5
	vmovdqu yword [edi + 32 * 6],	ymm6
	vmovdqu yword [edi + 32 * 7],	ymm7

	sub ecx, 32 * 8
	add edi, 32 * 8
	cmp ecx, 32 * 8
		jae cpy_32_8
align_nops
cpy_32_7:
	cmp ecx, 32 * 7
		jb cpy_32_6
	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1
	vmovdqu yword [edi + 32 * 2],	ymm2
	vmovdqu yword [edi + 32 * 3],	ymm3
	vmovdqu yword [edi + 32 * 4],	ymm4
	vmovdqu yword [edi + 32 * 5],	ymm5
	vmovdqu yword [edi + 32 * 6],	ymm6

	sub ecx, 32 * 7
	add edi, 32 * 7
align_nops
cpy_32_6:
	cmp ecx, 32 * 6
		jb cpy_32_5
	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1
	vmovdqu yword [edi + 32 * 2],	ymm2
	vmovdqu yword [edi + 32 * 3],	ymm3
	vmovdqu yword [edi + 32 * 4],	ymm4
	vmovdqu yword [edi + 32 * 5],	ymm5

	sub ecx, 32 * 6
	add edi, 32 * 6
align_nops
cpy_32_5:
	cmp ecx, 32 * 5
		jb cpy_32_4
	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1
	vmovdqu yword [edi + 32 * 2],	ymm2
	vmovdqu yword [edi + 32 * 3],	ymm3
	vmovdqu yword [edi + 32 * 4],	ymm4

	sub ecx, 32 * 5
	add edi, 32 * 5
align_nops
cpy_32_4:
	cmp ecx, 32 * 4
		jb cpy_32_3
	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1
	vmovdqu yword [edi + 32 * 2],	ymm2
	vmovdqu yword [edi + 32 * 3],	ymm3

	sub ecx, 32 * 4
	add edi, 32 * 4
align_nops
cpy_32_3:
	cmp ecx, 32 * 3
		jb cpy_32_2
	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1
	vmovdqu yword [edi + 32 * 2],	ymm2

	sub ecx, 32 * 3
	add edi, 32 * 3
align_nops
cpy_32_2:
	cmp ecx, 32 * 2
		jb cpy_32
	vmovdqu yword [edi + 32 * 0],	ymm0
	vmovdqu yword [edi + 32 * 1],	ymm1

	sub ecx, 32 * 2
	add edi, 32 * 2
align_nops
cpy_32:
	cmp ecx, 32
		jb cpy_16
	vmovdqu yword [edi + 32 * 0],	ymm0

	sub ecx, 32
	add edi, 32
align_nops
cpy_16:
	cmp ecx, 16
		jb cpy_8
	vmovdqu xword [edi], xmm0

	sub ecx, 16
	add edi, 16
align_nops
cpy_8:
	cmp ecx, 8
		jb cpy_4
	mov dword [edi + 4 * 0], 0xff252525
	mov dword [edi + 4 * 1], 0xff252525

	sub ecx, 8
	add edi, 8
align_nops
cpy_4:
	test ecx, ecx
		jz prepare_next_line
	mov dword [edi], 0xff252525

	add edi, 4
align_nops
prepare_next_line:
	dec ebx ; height--
		jz epilog
	add edi, esi ; adjust
	jmp next_line
align_nops
epilog:
	mov ebx, [ebx_save]
	mov edi, [edi_save]
	mov esi, [esi_save]
	mov ebp, [ebp_save]
	vzeroupper ; end of AVX2 code
	ret
end namespace ; clearcolor_avx2
;***************************************************************************************************
public minmax_avx2
align 16
minmax_avx2: namespace minmax_avx2
;---------------------------------------------------------------------------------------------------
; ctx
min			:= 0	; out
v0			:= 0
v0_plane_x 		:= 0
v0_plane_y		:= 1 * 4
v0_tex_s		:= 2 * 4
v0_tex_t		:= 3 * 4
max			:= 4 * 4	; out
v1			:= 4 * 4	; in
v1_plane_x 		:= 4 * 4
v1_plane_y		:= 5 * 4
v1_tex_s		:= 6 * 4
v1_tex_t		:= 7 * 4
v2			:= 8 * 4
v2_plane_x 		:= 8 * 4
v2_plane_y		:= 9 * 4
v2_tex_s		:= 10 * 4
v2_tex_t		:= 11 * 4
v3			:= 12 * 4
v3_plane_x 		:= 12 * 4
v3_plane_y		:= 13 * 4
v3_tex_s		:= 14 * 4
v3_tex_t		:= 15 * 4
minmax_scale		:= 16 * 4
minmax_scale_x		:= 16 * 4
minmax_scale_y		:= 17 * 4
minmax_scale_s		:= 18 * 4
minmax_scale_t		:= 19 * 4
;---------------------------------------------------------------------------------------------------
; stack
;--- caller
define ret_addr		esp + 4 * 0
define ctx		esp + 4 * 1
;---------------------------------------------------------------------------------------------------
	mov eax, dword [ctx] 

	vmovups xmm0, [eax + v0]
	vmovups xmm1, [eax + v1]
	vmovups xmm2, [eax + v2]
	vmovups xmm3, [eax + v3]
	vmovups xmm6, [eax + minmax_scale]

	vminps xmm5, xmm0, xmm1
	vminps xmm5, xmm5, xmm2
	vminps xmm5, xmm5, xmm3
	vcvtdq2ps xmm6, xmm6 ; to f32
	vmulps xmm5, xmm5, xmm6
	vcvtps2dq xmm5, xmm5
	vmovups [eax + min], xmm5

	vmaxps xmm4, xmm0, xmm1
	vmaxps xmm4, xmm4, xmm2
	vmaxps xmm4, xmm4, xmm3
	vmulps xmm4, xmm4, xmm6
	vcvtps2dq xmm4, xmm4
	vmovups [eax + max], xmm4

	vzeroupper ; end of AVX2 code
	ret
end namespace ; minmax_avx2
;***************************************************************************************************
; TODO: test if the steam client is actually using argb (the blue seems to go away)
public alphablend_rgba_avx2
align 16
alphablend_rgba_avx2: namespace alphablend_rgba_avx2
;---------------------------------------------------------------------------------------------------
; input 
dst			:= 0
dst_adjust_bytes_n	:= 4
src			:= 8
src_adjust_bytes_n	:= 12
width_pixs_n		:= 16
height_lines_n		:= 20
;---------------------------------------------------------------------------------------------------
; stack
;---- frame
define ebx_save		esp + 4 * -4
define edi_save		esp + 4 * -3
define esi_save		esp + 4 * -2
define ebp_save		esp + 4 * -1
;--- caller
define ret_addr		esp + 4 * 0
define input		esp + 4 * 1
;---------------------------------------------------------------------------------------------------
	mov [ebx_save], ebx
	mov [edi_save], edi
	mov [esi_save], esi
	mov [ebp_save], ebp
	mov ebx, dword [input] 

	mov edi, dword [ebx + dst]
	mov esi, dword [ebx + src]
	mov edx, dword [ebx + height_lines_n]

	; CONSTANTS -- START -----------------------------------------------------------------------
	;  0 1 2 3 4 5 6 7  8 9101112131415 1617181920212223 2425262728293031
	; ff00ff00ff00ff00 ff00ff00ff00ff00 ff00ff00ff00ff00 ff00ff00ff00ff00
	vpcmpeqb ymm7, ymm7, ymm7
	vpsrlw ymm7, ymm7, 8
	;  0 1 2 3 4 5 6 7  8 9101112131415 1617181920212223 2425262728293031
	; 000000ff000000ff 000000ff000000ff 000000ff000000ff 000000ff000000ff
	vpcmpeqb ymm6, ymm6, ymm6
	vpsrld ymm6, ymm6, 24
	vpslld ymm6, ymm6, 24

	mov eax, 0x808080 ; see below for why (maths)
	mov ecx, 0x807f807f ; see below for why (maths)
	vmovd xmm5, eax
	;  0 1 2 3 4 5 6 7  8 9101112131415 1617181920212223 2425262728293031
	; 8080800080808000 8080800080808000 8080800080808000 8080800080808000
	vpbroadcastd ymm5, xmm5
	vmovd xmm4, ecx
	;  0 1 2 3 4 5 6 7  8 9101112131415 1617181920212223 2425262728293031
	; 7f807f807f807f80 7f807f807f807f80 7f807f807f807f80 7f807f807f807f80
	vpbroadcastd ymm4, xmm4
	; CONSTANTS -- END -------------------------------------------------------------------------
align_nops
next_line:
	mov ecx, dword [ebx + width_pixs_n]

	cmp ecx, 4
		jb blend_2pixs
align_nops	
blend_4pixs:
	; load 4 pixels from the src and 4 pixels from the dst
	vmovdqu xmm0, xword [edi] ; clear hi xmm from ymm
	vmovdqu xmm1, xword [esi] ; clear hi xmm from ymm
	; from positive to translated and signed values b = B - 0x7f, but _not_ for alpha
	; WE DO THAT BECAUSE WE WILL USE THE VPMADDUBSW INSTRUCTION BELOW AND A XOR 0xFF
	;        0 1 2 3 4 5 6 7  8 9101112131415 1617181920212223 2425262728293031
	; from: R0G0B0A0R1G1B1A1 R2G2B2A2R3G3B3A3 0000000000000000 0000000000000000
	; to:   r0g0b0a0b1g1r1A1 r2g2b2a2r3g3b3A3 0000000000000000 0000000000000000
	vpsubb xmm0, xmm0, xmm5
	vpsubb xmm1, xmm1, xmm5
	;        0 1 2 3 4 5 6 7  8 9101112131415 1617181920212223 2425262728293031
	; from: r0g0b0a0r1g1b1A1 r2g2b2a2r3g3b3A3 0000000000000000 0000000000000000
	; to:   r0g0b0a0r1g1b1A1 0000000000000000 r2g2b2a2r3g3b3A3 0000000000000000
	vpermq ymm0, ymm0, 10011000b
	vpermq ymm1, ymm1, 10011000b
	; f->framebuffer, t->texture
	;         0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31
	; from: r0f g0f b0f A0f r1f g1f b1f A1f 00  00  00  00  00  00  00  00  r2f g2f b2f A2f r3f g3f b3f A3f 00  00  00  00  00  00  00  00 
	;       r0t g0t b0t A0t r1t g1t b1t A1t 00  00  00  00  00  00  00  00  r2t g2t b2t A2t r3t g3t b3t A3t 00  00  00  00  00  00  00  00 
	; to:   r0f r0t g0f g0t b0f b0t A0f A0t r1f r1t g1f g1t b1f b1t A1f A1t r2f r2t g2f g2t b2f b2t a2f A2t r3f r3t g3f g3t b3f b3t A3f A3t
	vpunpcklbw ymm0, ymm0, ymm1
	; rgba -> bgra, switch r and b
	;           0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31
	; from:   r0f b0t g0f g0t b0f b0t A0f A0t r1f r1t g1f g1t b1f b1t A1f A1t r2f r2t g2f g2t b2f b2t a2f A2t r3f r3t g3f g3t b3f b3t A3f A3t
	; to:     b0f b0t g0f g0t r0f r0t A0f A0t b1f b1t g1f g1t r1f r1t A1f A1t b2f b2t g2f g2t r2f r2t a2f A2t b3f b3t g3f g3t r3f r3t A3f A3t
	vpshuflw ymm0, ymm0, 11000110b
	vpshufhw ymm0, ymm0, 11000110b
	;         0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31
	; from: b0f b0t g0f g0t r0f r0t A0f A0t b1f b1t g1f g1t r1f r1t A1f A1t b2f b2t g2f g2t r2f r2t A2f A2t b3f b3t g3f g3t r3f r3t A3f A3t
	; to:   a0t 00  00  00  00  00  00  00  a1t 00  00  00  00  00  00  00  a2t 00  00  00  00  00  00  00  a3t 00  00  00  00  00  00  00 
	vpsrlq ymm1, ymm0, 56
	;         0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31
	; from: A0t 00  00  00  00  00  00  00  A1t 00  00  00  00  00  00  00  A2t 00  00  00  00  00  00  00  A3t 00  00  00  00  00  00  00 
	; to:   00  A0t 00  00  00  00  00  00  00  A1t 00  00  00  00  00  00  00  A2t 00  00  00  00  00  00  00  A3t 00  00  00  00  00  00 
	vpsllq ymm2, ymm1, 8
	;         0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15 16  17  18  19  20  21  22  23 24  25  26  27  28  29  30  31
	; from: A0t 00  00  00  00  00  00  00  A1t 00  00  00  00  00  00  00 A2t 00  00  00  00  00  00  00 A3t 00  00  00  00  00  00  00 
	; to:   A0t A0t 00  00  00  00  00  00  A1t A1t 00  00  00  00  00  00 A2t A2t 00  00  00  00  00  00 A3t A3t 00  00  00  00  00  00 
	vpor ymm1, ymm2, ymm1
	;         0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15 16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31
	; from: A0t A0t 00  00  00  00  00  00  A1t A1t 00  00  00  00  00  00 A2t A2t 00  00  00  00  00  00  A3t A3t 00  00  00  00  00  00 
	; to:   A0t A0t A0t A0t A0t A0t A0t A0t A1t A1t 00  00  00  00  00  00 A2t A2t A2t A2t A2t A2t d2f A2t A3f A3t 00  00  00  00  00  00
	vpshuflw ymm1, ymm1, 0
	;         0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31
	; from: A0t A0t A0t A0t A0t A0t A0t A0t A1t A1t 00  00  00  00  00  00  A2t A2t A2t A2t A2t A2t A2t A2t A3t A3t 00  00  00  00  00  00
	; to:   A0t A0t A0t A0t A0t A0t A0t A0t A1t A1t A1t A1t A1t A1t A1t A1t A2t A2t A2t A2t A2t A2t A2t A2t A3t A3t A3t A3t A3t A3t A3t A3t 
	vpshufhw ymm1, ymm1, 0
	;         0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31
	; from: A0t A0t A0t A0t A0t A0t A0t A0t A1t A1t A1t A1t A1t A1t A1t A1t A2t A2t A2t A2t A2t A2t A2t A2t A3t A3t A3t A3t A3t A3t A3t A3t 
	;       ff  00  ff  00  ff  00  ff  00  ff  00  ff  00  ff  00  ff  00  ff  00  ff  00  ff  00  ff  00  ff  00  ff  00  ff  00  ff  00
	; to:   D = (255 - A) = xor A, 0xff
	;       D0t A0t D0t A0t D0t A0t D0t A0t D1t A1t D1t A1t D1t A1t D1t A1t D2t A2t D2t A2t D2t A2t D2t A2t D3t A3t D3t A3t D3t A3t D3t A3t 
	vpxor ymm1, ymm1, ymm7

	; integer pixel alpha blending with 2^8 = 0x100 divisor instead of 0xff, on a 16bits scale
	; 	F = (F * (0xff - A) + T * A + 0xff) >> 8
	; BUT we are working with f and t which are:
	; 	f = (F - 0x80) and t = (T - 0x80)
	; If you do the maths, you get:
	; 	F = f * (0xff * A) + t * A + 0x807f (0xff * 0x80) + 0xff
	;							= f * (0xff * A) + t * A + 0x807f

	; f * (0xff -a) + t * a
	;                         0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31
	; src1(unsigned 8bits): D0f A0t D0f A0t D0f A0t D0f A0t D1f A1t D1f A1t D1f A1t D1f A1t D2f A2t D2f A2t D2f A2t D2f A2t D3f A3t D3f A3t D3f A3t D3f A3t 
	; src2(signed 8bits):   b0f b0t g0f g0t r0f r0t A0f A0t b1f b1t g1f g1t r1f r1t A1f A1t b2f b2t g2f g2t r2f r2t A2f A2t b3f b3t g3f g3t r3f r3t A3f A3t
	; to:                   b0----- g0----- r0----- a0----- b1----- g1----- r1----- a1----- b2----- g2----- r2----- a1----- b3----- g3----- r3----- a3-----
	vpmaddubsw ymm0, ymm1, ymm0 ; ymm1 is unsigned, ymm0 is signed
	; + 0x807f
	vpaddw ymm0, ymm0, ymm4
	; >> 8
	vpsrlw ymm0, ymm0, 8
	; we have to do it this way because of the weird handling of ymm regs by vpackuswb
	;         0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31
	; from: b0----- g0----- r0----- A0----- b1----- g1----- r1----- A1-----
	; from: b2----- g2----- r2----- A1----- b3----- g3----- r3----- A3-----
	; to:   b0  g0  r0  A0  b1  g1  r1  A1  b2  g2  r2  A2  b3  g3  r3  A3
	vextracti128 xmm1, ymm0, 1
	vpackuswb xmm0, xmm0, xmm1
	;         0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31
	; from: b0  g0  r0  A0  b1  g1  r1  A1  b2  g2  r2  A2  b3  g3  r3  A3  b0  g0  r0  A0  b1  g1  r1  A1  b2  g2  r2  A2  b3  g3  r3  A3
	; to:   b0  g0  r0  ff  b1  g1  r1  ff  b2  g2  r2  ff  b3  g3  r3  ff  b0  g0  r0  A0  b1  g1  r1  ff  b2  g2  r2  ff  b3  g3  r3  ff
	vpor xmm0, xmm0, xmm6

	vmovdqu [edi], xmm0

	sub ecx, 4 ; pixs
	add edi, 4 * 4 ; bytes
	add esi, 4 * 4 ; bytes
	cmp ecx, 4
		jae blend_4pixs
align_nops
blend_2pixs:
	cmp ecx, 2
		jb blend_1pixs
	vmovq xmm0, qword [edi] ; zero extended
	vmovq xmm1, qword [esi] ; zero extended
	vpsubb xmm0, xmm0, xmm5
	vpsubb xmm1, xmm1, xmm5
	vpunpcklbw xmm0, xmm0, xmm1
	vpshuflw xmm0, xmm0, 11000110b
	vpsrlq xmm1, xmm0, 56
	vpsllq xmm2, xmm1, 8
	vpor xmm1, xmm2, xmm1
	vpshuflw xmm1, xmm1, 0
	vpshufhw xmm1, xmm1, 0
	vpxor xmm1, xmm1, xmm7
	vpmaddubsw xmm0, xmm1, xmm0 ; xmm1 is unsigned, xmm0 is signed
	vpaddw xmm0, xmm0, xmm4
	vpsrlw xmm0, xmm0, 8
	vpackuswb xmm0, xmm0, xmm0
	vpor xmm0, xmm0, xmm6
	vmovq qword [edi], xmm0

	sub ecx, 2 ; pixs
	add edi, 2 * 4 ; bytes
	add esi, 2 * 4 ; bytes
align_nops
blend_1pixs:
	test ecx, ecx
		jz prepare_next_line
	vmovd xmm0, dword [edi] ; zero extended
	vmovd xmm1, dword [esi] ; zero extended
	vpsubb xmm0, xmm0, xmm5
	vpsubb xmm1, xmm1, xmm5
	vpunpcklbw xmm0, xmm0, xmm1
	vpshuflw xmm0, xmm0, 11000110b
	vpsrlq xmm1, xmm0, 56
	vpsllq xmm2, xmm1, 8
	vpor xmm1, xmm2, xmm1
	vpshuflw xmm1, xmm1, 0
	vpshufhw xmm1, xmm1, 0
	vpxor xmm1, xmm1, xmm7
	vpmaddubsw xmm0, xmm1, xmm0 ; xmm1 is unsigned, xmm0 is signed
	vpaddw xmm0, xmm0, xmm4
	vpsrlw xmm0, xmm0, 8
	vpackuswb xmm0, xmm0, xmm0
	vpor xmm0, xmm0, xmm6
	vmovd dword [edi], xmm0
	add edi, 4 ; bytes
	add esi, 4 ; bytes
align_nops
prepare_next_line:
	add edi, dword [ebx + dst_adjust_bytes_n]
	add esi, dword [ebx + src_adjust_bytes_n]
	dec edx ; lines--
		jnz next_line
align_nops
epilog:
	mov ebx, [ebx_save]
	mov edi, [edi_save]
	mov esi, [esi_save]
	mov ebp, [ebp_save]
	vzeroupper ; end of AVX2 code
	ret
end namespace ; alphablend_bgra_avx2
;***************************************************************************************************
macro align_nops
	local a
	virtual
		align 16
		a = $ - $$
	end virtual
	if a = 15
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
		db 0x66, 0xf, 0x1f, 0x44, 0x00, 0x00
	else if a = 14
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
		db 0xf, 0x1f, 0x44, 0x00, 0x00
	else if a = 13
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
		db 0xf, 0x1f, 0x40, 0x00
	else if a = 12
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
		db 0xf, 0x1f, 0x00
	else if a = 11
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
		db 0x66, 0x90
	else if a = 10
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
		db 0x90
	else if a = 9
		db 0x66, 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
	else if a = 8
		db 0xf, 0x1f, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00
	else if a = 7
		db 0xf, 0x1f, 0x80, 0x00, 0x00, 0x00, 0x00
	else if a = 6
		db 0x66, 0xf, 0x1f, 0x44, 0x00, 0x00
	else if a = 5
		db 0xf, 0x1f, 0x44, 0x00, 0x00
	else if a = 4
		db 0xf, 0x1f, 0x40, 0x00
	else if a = 3
		db 0xf, 0x1f, 0x00
	else if a = 2
		db 0x66, 0x90
	else if a = 1
		db 0x90
	end if
end macro
