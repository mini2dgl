#!/bin/sh

# this is actually UN-TESTED, this is supposely working on massive
# mainstream distros.
# (we use nyan.sh where almost everything is in isolation)
# (this code should be decent enough to work with naive and simple C compilers,
# like tinycc)

# the following should be something like "/home/user/.local/share/"
steam_install_dir=FILL_IN_WITH_YOUR_STEAM_INSTALL_DIRECTORY

AVX2_32BITS_CPPFLAGS=-DAVX2_32BITS
#AVX2_32BITS_CPPFLAGS=

if test "x$AVX2_32BITS_CPPFLAGS" != x; then
	AVX2_32BITS_LDFLAGS=/run/user/$(id -u)/avx2.o
	export INCLUDE=$(readlink -f ./fasm/examples/x86/include/)
	./fasm/fasmg.x64 ./avx2.s $AVX2_32BITS_LDFLAGS
	unset INCLUDE
fi

gcc -O2 -fPIC -pipe -shared -static-libgcc \
	$AVX2_32BITS_CPPFLAGS \
	main.c \
	$AVX2_32BITS_LDFLAGS \
	-o libGL.so.1 \
	-Wl,--version-script=libgl.map \
	$steam_install_dir/Steam/ubuntu12_32/steam-runtime/usr/lib/i386-linux-gnu/libX11.so.6 \
	$steam_install_dir/Steam/ubuntu12_32/steam-runtime/usr/lib/i386-linux-gnu/libXext.so.6


# steam client bug: forcing libva + libdrm (would not work on any solo
# software libGL install)
gcc -O2 -fPIC -pipe -shared -static-libgcc \
	drm.c \
	-o libdrm.so.2 \
	-Wl,--version-script=drm.map \
	$steam_install_dir/Steam/ubuntu12_32/steam-runtime/usr/lib/i386-linux-gnu/libX11.so.6 \
	$steam_install_dir/Steam/ubuntu12_32/steam-runtime/usr/lib/i386-linux-gnu/libXext.so.6
