#!/bin/sh

# this is the build script for our own distro where almost everything is
# isolated
AVX2_32BITS_CPPFLAGS=-DAVX2_32BITS
#AVX2_32BITS_CPPFLAGS=

/run/x86/cross-compiler/bin/i686-pc-linux-gnu-gcc \
	-c -O2 -fPIC -pipe \
	-I./include \
	-I/nyan/libXext/current/include \
	-I/nyan/libX11/current/include \
	-I/nyan/xorgproto/current/include \
	-idirafter /opt/steam/x86/devel/include \
	-idirafter /nyan/linux-headers/current/include \
	-static-libgcc \
	$AVX2_32BITS_CPPFLAGS \
	-o /run/main.o \
	./main.c

if test "x$AVX2_32BITS_CPPFLAGS" != x; then
	AVX2_32BITS_LDFLAGS=/run/avx2.o
	export INCLUDE=$(readlink -f ./fasm/examples/x86/include/)
	./fasm/fasmg.x64 ./avx2.s $AVX2_32BITS_LDFLAGS
	unset INCLUDE
fi

/run/x86/cross-compiler/bin/i686-pc-linux-gnu-gcc \
	-shared \
	-Wl,--version-script=libgl.map \
	-o /opt/steam/x86/lib/libGL.so.1 \
	/run/main.o \
	$AVX2_32BITS_LDFLAGS \
	-B/opt/steam/x86/devel/lib \
	-B/opt/steam/x86/lib \
	-lc \
	-static-libgcc \
	/home/user/.local/share/Steam/ubuntu12_32/steam-runtime/usr/lib/i386-linux-gnu/libX11.so.6 \
	/home/user/.local/share/Steam/ubuntu12_32/steam-runtime/usr/lib/i386-linux-gnu/libXext.so.6
#-------------------------------------------------------------------------------
/run/x86/cross-compiler/bin/i686-pc-linux-gnu-gcc \
	-c -O2 -fPIC -pipe \
	-static-libgcc \
	-o /run/drm.o \
	./drm.c

/run/x86/cross-compiler/bin/i686-pc-linux-gnu-gcc \
	-shared \
	-Wl,--version-script=drm.map \
	-o /opt/steam/x86/lib/libdrm.so.2 \
	/run/drm.o \
	-B/opt/steam/x86/devel/lib \
	-B/opt/steam/x86/lib \
	-static-libgcc \
	-lc
