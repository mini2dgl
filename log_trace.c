#ifndef TRACE_C
#define TRACE_C
#include <stdarg.h>
#include <stdio.h>
/* TRACE {{{1 */
/*############################################################################*/
#ifndef TRACE_FILE
#define TRACE_FILE "/run/log"
#endif
#define TRACE(...) trace((void*)__func__, __VA_ARGS__)
#ifdef TRACE_ON
#include <sys/file.h>
static FILE *trace_file;
static void trace_init(void)
{
	trace_file = fopen(TRACE_FILE, "a");
	setvbuf(trace_file, NULL, _IONBF, 0);
	fcntl(fileno(trace_file), F_SETFL, fcntl(fileno(trace_file),
							F_GETFL) | O_SYNC);
}
static void trace(void *fn, void *fmt, ...)
{
	int r;
	size_t sz;
	va_list ap;

	flock(fileno(trace_file), LOCK_EX); /* process level */
	flockfile(trace_file); /* thread level */
	fprintf(trace_file, "%d:%d:%s:", getpid(), gettid(), fn);
	va_start(ap, fmt);
	vfprintf(trace_file, fmt, ap);
	va_end(ap);
	fprintf(trace_file, "\n");
	fflush(trace_file);
	funlockfile(trace_file); /* thread level */
	flock(fileno(trace_file), LOCK_UN); /* process level */
}
#define STRING(x) case x: return #x
static void *getstring_str(unsigned int e)
{
	switch(e) {
	STRING(GL_VENDOR);
	STRING(GL_RENDERER);
	STRING(GL_VERSION);
	STRING(GL_SHADING_LANGUAGE_VERSION);
	/* unavailable thru glGetString in core profile */
	STRING(GL_EXTENSIONS);
	default:
		return "UNKNOWN";
	}

}
#undef STRING
#define TARGET(x) case x: return #x
static void *tex_target_str(GLenum target)
{
	switch(target) {
	TARGET(GL_TEXTURE_2D);
	default:
		return "UNKNOWN";
	}
}
#undef TARGET
#define MODE_STR(x) case x: return #x
static void *matrixmode_mode_str(unsigned int mode)
{
	switch(mode) {
	MODE_STR(GL_MODELVIEW);
	MODE_STR(GL_PROJECTION);
	MODE_STR(GL_TEXTURE);
	MODE_STR(GL_COLOR);
	default:
		return "UNKNOWN";
	}
}
#undef MODE_STR
#define TARGET(x) case x: return #x
static void *texenv_target_str(unsigned int t)
{
	switch(t) {
	TARGET(GL_TEXTURE_ENV);
	TARGET(GL_TEXTURE_FILTER_CONTROL);
	TARGET(GL_POINT_SPRITE);
	default:
		return "UNKNOWN";
	}
}
#undef TARGET
#define PNAME(x) case x: return #x
static void *texenv_pname_str(unsigned int pname)
{
	switch(pname) {
	PNAME(GL_TEXTURE_ENV_MODE);
	default:
		return "UNKNOWN";
	}
}
#undef PNAME
#define MODE(x) case x: return #x
static void *rendermode_str(unsigned int mode)
{
	switch(mode) {
	MODE(GL_RENDER);
	MODE(GL_FEEDBACK);
	MODE(GL_SELECT);
	default:
		return "UNKNOWN";
	}
}
#undef MODE
#define PNAME(x) case x: return #x
static void *getinteger_pname_str(unsigned int pname)
{
	switch(pname) {
	PNAME(GL_VIEWPORT);
	PNAME(GL_NUM_EXTENSIONS);
	default:
		return "UNKNOWN";
	}
}
#undef PNAME
#define FMT(x) case x: return #x
static void *tex_format_str(GLint fmt)
{
	switch(fmt) {
	FMT(GL_RGBA8);
	FMT(GL_BGRA);
	FMT(GL_RGBA);
	default:
		return "UNKNOWN";
	}
}
#undef FMT
#define TYPE(x) case x: return #x
static void *pixel_data_format_str(GLenum type)
{
	switch(type) {
	TYPE(GL_UNSIGNED_BYTE);
	default:
		return "UNKNOWN";
	}
}
#undef TYPE
#define PNAME(x) case x: return #x
static void *pixelstore_pname_str(GLenum pname)
{
	switch(pname) {
	PNAME(GL_PACK_ALIGNMENT); /* alignment to write data to client */
	PNAME(GL_PACK_SWAP_BYTES);
	PNAME(GL_PACK_LSB_FIRST);
	PNAME(GL_PACK_ROW_LENGTH);
	PNAME(GL_PACK_IMAGE_HEIGHT);
	PNAME(GL_PACK_SKIP_PIXELS);
	PNAME(GL_PACK_SKIP_ROWS);
	PNAME(GL_PACK_SKIP_IMAGES);
	PNAME(GL_UNPACK_ALIGNMENT); /* alignment to read data from client */
	PNAME(GL_UNPACK_SWAP_BYTES);
	PNAME(GL_UNPACK_LSB_FIRST);
	PNAME(GL_UNPACK_ROW_LENGTH);
	PNAME(GL_UNPACK_IMAGE_HEIGHT);
	PNAME(GL_UNPACK_SKIP_PIXELS);
	PNAME(GL_UNPACK_SKIP_ROWS);
	default:
		return "UNKNOWN";
	}
}
#undef PNAME 
#define MODE(x) case x: return #x
static void *begin_mode(GLenum mode)
{
	switch(mode) {
	MODE(GL_POINTS);
	MODE(GL_LINES);
	MODE(GL_LINE_LOOP);
	MODE(GL_LINE_STRIP);
	MODE(GL_TRIANGLES);
	MODE(GL_TRIANGLE_STRIP);
	MODE(GL_TRIANGLE_FAN);
	MODE(GL_QUADS);
	MODE(GL_QUAD_STRIP);
	MODE(GL_POLYGON);
	default:
		return "UNKNOWN";
	}
}
#undef MODE
#define PNAME(x) case x: return #x
static void *texparameter_pname_str(GLenum pname)
{
	switch(pname) {
	PNAME(GL_DEPTH_STENCIL_TEXTURE_MODE);
	PNAME(GL_TEXTURE_BASE_LEVEL);
	PNAME(GL_TEXTURE_COMPARE_FUNC);
	PNAME(GL_TEXTURE_COMPARE_MODE);
	PNAME(GL_TEXTURE_LOD_BIAS);
	PNAME(GL_TEXTURE_MIN_FILTER);
	PNAME(GL_TEXTURE_MAG_FILTER);
	PNAME(GL_TEXTURE_MIN_LOD);
	PNAME(GL_TEXTURE_MAX_LOD);
	PNAME(GL_TEXTURE_MAX_LEVEL);
	PNAME(GL_TEXTURE_SWIZZLE_R);
	PNAME(GL_TEXTURE_SWIZZLE_G);
	PNAME(GL_TEXTURE_SWIZZLE_B);
	PNAME(GL_TEXTURE_SWIZZLE_A);
	PNAME(GL_TEXTURE_WRAP_S);
	PNAME(GL_TEXTURE_WRAP_T);
	PNAME(GL_TEXTURE_WRAP_R);
	default:
		return "UNKNOW";
	}
}
#undef PNAME
static void *texparameterf_param_str(GLenum pname, GLfloat param)
{
	if (pname == GL_TEXTURE_WRAP_S || pname == GL_TEXTURE_WRAP_T
							|| GL_TEXTURE_WRAP_R) {
		int param_u;

		param_u = (int)param;
		#define WRAP(x) case x: return #x
		switch(param_u) {
		WRAP(GL_CLAMP_TO_EDGE);
		WRAP(GL_CLAMP_TO_BORDER);
		WRAP(GL_MIRRORED_REPEAT);
	 	WRAP(GL_REPEAT);
		WRAP(GL_MIRROR_CLAMP_TO_EDGE);
		default:
			return "UNKOWN";
		}
		#undef WRAP
	}
	return "UNKNOWN";
}
#define CAP(x) case x: return #x
static void *disable_enable_cap_str(GLenum cap)
{
	switch(cap) {
	CAP(GL_ALPHA_TEST);
	CAP(GL_AUTO_NORMAL);
	CAP(GL_BLEND);
	CAP(GL_CLIP_PLANE0);
	CAP(GL_CLIP_PLANE1);
	CAP(GL_CLIP_PLANE2);
	CAP(GL_CLIP_PLANE3);
	CAP(GL_CLIP_PLANE4);
	CAP(GL_CLIP_PLANE5);
	CAP(GL_COLOR_LOGIC_OP);
	CAP(GL_COLOR_MATERIAL);
	CAP(GL_COLOR_SUM);
	CAP(GL_COLOR_TABLE);
	CAP(GL_CONVOLUTION_1D);
	CAP(GL_CONVOLUTION_2D);
	CAP(GL_CULL_FACE);
	CAP(GL_DEPTH_TEST);
	CAP(GL_DITHER);
	CAP(GL_FOG);
	CAP(GL_HISTOGRAM);
	CAP(GL_INDEX_LOGIC_OP);
	CAP(GL_LIGHT0);
	CAP(GL_LIGHT1);
	CAP(GL_LIGHT2);
	CAP(GL_LIGHT3);
	CAP(GL_LIGHT4);
	CAP(GL_LIGHT5);
	CAP(GL_LIGHT6);
	CAP(GL_LIGHT7);
	CAP(GL_LIGHTING);
	CAP(GL_LINE_SMOOTH);
	CAP(GL_LINE_STIPPLE);
	CAP(GL_MAP1_COLOR_4);
	CAP(GL_MAP1_INDEX);
	CAP(GL_MAP1_NORMAL);
	CAP(GL_MAP1_TEXTURE_COORD_1);
	CAP(GL_MAP1_TEXTURE_COORD_2);
	CAP(GL_MAP1_TEXTURE_COORD_3);
	CAP(GL_MAP1_TEXTURE_COORD_4);
	CAP(GL_MAP1_VERTEX_3);
	CAP(GL_MAP1_VERTEX_4);
	CAP(GL_MAP2_COLOR_4);
	CAP(GL_MAP2_INDEX);
	CAP(GL_MAP2_NORMAL);
	CAP(GL_MAP2_TEXTURE_COORD_1);
	CAP(GL_MAP2_TEXTURE_COORD_2);
	CAP(GL_MAP2_TEXTURE_COORD_3);
	CAP(GL_MAP2_TEXTURE_COORD_4);
	CAP(GL_MAP2_VERTEX_3);
	CAP(GL_MAP2_VERTEX_4);
	CAP(GL_MINMAX);
	CAP(GL_MULTISAMPLE);
	CAP(GL_NORMALIZE);
	CAP(GL_POINT_SMOOTH);
	CAP(GL_POINT_SPRITE);
	CAP(GL_POLYGON_OFFSET_FILL);
	CAP(GL_POLYGON_OFFSET_LINE);
	CAP(GL_POLYGON_OFFSET_POINT);
	CAP(GL_POLYGON_SMOOTH);
	CAP(GL_POLYGON_STIPPLE);
	CAP(GL_POST_COLOR_MATRIX_COLOR_TABLE);
	CAP(GL_POST_CONVOLUTION_COLOR_TABLE);
	CAP(GL_RESCALE_NORMAL);
	CAP(GL_SAMPLE_ALPHA_TO_COVERAGE);
	CAP(GL_SAMPLE_ALPHA_TO_ONE);
	CAP(GL_SAMPLE_COVERAGE);
	CAP(GL_SEPARABLE_2D);
	CAP(GL_SCISSOR_TEST);
	CAP(GL_STENCIL_TEST);
	CAP(GL_TEXTURE_1D);
	CAP(GL_TEXTURE_2D);
	CAP(GL_TEXTURE_3D);
	CAP(GL_TEXTURE_CUBE_MAP);
	CAP(GL_TEXTURE_GEN_Q);
	CAP(GL_TEXTURE_GEN_R);
	CAP(GL_TEXTURE_GEN_S);
	CAP(GL_TEXTURE_GEN_T);
	CAP(GL_VERTEX_PROGRAM_POINT_SIZE);
	CAP(GL_VERTEX_PROGRAM_TWO_SIDE);
	CAP(GL_VERTEX_PROGRAM_ARB);
	CAP(GL_FRAGMENT_PROGRAM_ARB);
	CAP(GL_COLOR_ARRAY);
	CAP(GL_EDGE_FLAG_ARRAY);
	CAP(GL_INDEX_ARRAY);
	CAP(GL_NORMAL_ARRAY);
	CAP(GL_TEXTURE_COORD_ARRAY);
	CAP(GL_VERTEX_ARRAY);
	default:
		return "UNKNOWN";
	}
}
#define MODE(x) case x: return #x
static void *shademodel_mode_str(GLenum mode)
{
	switch(mode) {
	MODE(GL_FLAT);
	MODE(GL_SMOOTH);
	default:
		return "UNKNOWN";
	}
}
#undef MODE
#define FACTOR(x) case x: return #x
static void *blendfunc_factor_str(GLenum factor)
{
	switch(factor) {
	FACTOR(GL_ZERO);
	FACTOR(GL_ONE);
	FACTOR(GL_SRC_COLOR);
	FACTOR(GL_ONE_MINUS_SRC_COLOR);
	FACTOR(GL_DST_COLOR);
	FACTOR(GL_ONE_MINUS_DST_COLOR);
	FACTOR(GL_SRC_ALPHA);
	FACTOR(GL_ONE_MINUS_SRC_ALPHA);
	FACTOR(GL_DST_ALPHA);
	FACTOR(GL_ONE_MINUS_DST_ALPHA);
	FACTOR(GL_CONSTANT_COLOR);
	FACTOR(GL_ONE_MINUS_CONSTANT_COLOR);
	FACTOR(GL_CONSTANT_ALPHA);
	FACTOR(GL_ONE_MINUS_CONSTANT_ALPHA);
	FACTOR(GL_SRC_ALPHA_SATURATE);
	FACTOR(GL_SRC1_COLOR);
	FACTOR(GL_ONE_MINUS_SRC1_COLOR);
	FACTOR(GL_SRC1_ALPHA);
	FACTOR(GL_ONE_MINUS_SRC1_ALPHA);
	default:
		return "UNKNOWN";
	}
}
#undef FACTOR
static void *texenv_param_str(unsigned int pname, float param)
{
	if (param == (float)GL_MODULATE)
		return "GL_MODULATE";
	return "UNKNOWN";
}
static void printf_visualinfo(const void *str, XVisualInfo *i)
{
	TRACE("%s:visualid=%lu", str, i->visualid);
	TRACE("%s:red_mask=%x", str, i->red_mask);
	TRACE("%s:green_mask=%x", str, i->green_mask);
	TRACE("%s:blue_mask=%x", str, i->blue_mask);
	TRACE("%s:class=%d", str, i->class);
	TRACE("%s:bits_per_rgb=%d", str, i->bits_per_rgb);
	TRACE("%s:depth=%u", str, i->depth);
	TRACE("%s:colormap_size=%u", str, i->colormap_size);
	TRACE("%s:screen=%d", str, i->screen);
}
static void attribList_parse(int *l)
{
	if (l == 0)
		return;
	loop {
		if (*l == None)
			return;
		switch(*l) {
		case GLX_USE_GL:
			TRACE("attriblist:GLX_USE_GL");
			++l;
			break;
		case GLX_BUFFER_SIZE:
			TRACE("attriblist:GLX_BUFFER_SIZE=%d", l[1]);
			l += 2;
			break;
		case GLX_LEVEL:
			TRACE("attriblist:GLX_LEVEL=%d", l[1]);
			l += 2;
			break;
		case GLX_RGBA:
			TRACE("attriblist:GLX_RGBA");
			++l;
			break;
		case GLX_DOUBLEBUFFER:
			TRACE("attriblist:GLX_DOUBLEBUFFER");
			++l;
			break;
		case GLX_STEREO:
			TRACE("attriblist:GLX_STEREO");
			++l;
			break;
		case GLX_AUX_BUFFERS:
			TRACE("attriblist:GLX_AUX_BUFFERS=%d", l[1]);
			l += 2;
			break;
		case GLX_RED_SIZE:
			TRACE("attriblist:GLX_RED_SIZE=%d bits", l[1]);
			l += 2;
			break;
		case GLX_GREEN_SIZE:
			TRACE("attriblist:GLX_GREEN_SIZE=%d bits", l[1]);
			l += 2;
			break;
		case GLX_BLUE_SIZE:
			TRACE("attriblist:GLX_BLUE_SIZE=%d bits", l[1]);
			l += 2;
			break;
		case GLX_ALPHA_SIZE:
			TRACE("attriblist:GLX_ALPHA_SIZE=%d bits", l[1]);
			l += 2;
			break;
		case GLX_DEPTH_SIZE:
			TRACE("attriblist:GLX_DEPTH_SIZE=%d", l[1]);
			l += 2;
			break;
		case GLX_STENCIL_SIZE:
			TRACE("attriblist:GLX_STENCIL_SIZE=%d", l[1]);
			l += 2;
			break;
		case GLX_ACCUM_RED_SIZE:
			TRACE("attriblist:GLX_ACCUM_RED_SIZE=%d", l[1]);
			l += 2;
			break;
		case GLX_ACCUM_GREEN_SIZE:
			TRACE("attriblist:GLX_ACCUM_GREEN_SIZE=%d", l[1]);
			l += 2;
			break;
		case GLX_ACCUM_BLUE_SIZE:
			TRACE("attriblist:GLX_ACCUM_BLUE_SIZE=%d", l[1]);
			l += 2;
			break;
		case GLX_ACCUM_ALPHA_SIZE:
			TRACE("attriblist:GLX_ACCUM_ALPHA_SIZE=%d", l[1]);
			l += 2;
			break;
		default:
			TRACE("attriblist:UNKNOWN=%d", *l);
			break;
		}
	}
}
#undef TRACE_FILE
#else /* !TRACE_ON */
/*
 * we rely on dead code elimination... mmmmmh... we don't know if it is 
 * a feature expensive for a naive and simple C89(with bits of C99) compiler
 */
static void trace_init(){};
static void trace(void *fn, void *fmt, ...){}
static void attribList_parse(int *l){}
static void printf_visualinfo(const void *str, XVisualInfo *i){}
static void *getstring_str(unsigned int e){}
static void *tex_target_str(GLenum target){}
static void *matrixmode_mode_str(unsigned int mode){}
static void *texenv_target_str(unsigned int t){}
static void *texenv_pname_str(unsigned int pname){}
static void *rendermode_str(unsigned int mode){}
static void *getinteger_pname_str(unsigned int pname){}
static void *tex_format_str(GLint fmt){}
static void *pixel_data_format_str(GLenum type){}
static void *pixelstore_pname_str(GLenum pname){}
static void *begin_mode(GLenum mode){}
static void *texparameter_pname_str(GLenum pname){}
static void *texparameterf_param_str(GLenum pname, GLfloat param){}
static void *disable_enable_cap_str(GLenum cap){}
static void *shademodel_mode_str(GLenum mode){}
static void *blendfunc_factor_str(GLenum factor){}
static void *texenv_param_str(unsigned int pname, float param){}
#endif
/*############################################################################*/
/* }}} */
/* LOG {{{1 */
/*############################################################################*/
#define LOG(...) vlog((void*)__func__, __VA_ARGS__)
static void vlog(void *fn, void *fmt, ...)
{
	va_list ap;
#if defined(TRACE_ON) && defined(REDIRECT_LOG_TO_TRACE_FILE)
	flock(fileno(trace_file), LOCK_EX); /* process level */
	flockfile(trace_file); /* thread level */
	fprintf(trace_file, "%d:%s:", getpid(), fn);
	va_start(ap, fmt);
	vfprintf(trace_file, fmt, ap);
	va_end(ap);
	fprintf(trace_file, "\n");
	fflush(trace_file);
	funlockfile(trace_file); /* thread level */
	flock(fileno(trace_file), LOCK_UN); /* process level */
#else
	flockfile(stderr); /* thread level */
	fprintf(stderr, "MINI2DGL:%s:", fn);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fprintf(stderr, "\n");
	fflush(stderr);
	funlockfile(stderr); /* thread level */
#endif
}
/*############################################################################*/
/* }}} */
#endif
