#ifndef GL_C
#define GL_C
/*{{{*/const GLubyte *glGetString(GLenum name)
{
	void *r;
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("0x%x(%s)", name, getstring_str(name));
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = 0;
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("self thread has no current context");
		r =  0;
		goto leave;
	}
	switch(name) {
	case GL_VENDOR:
		r = "nyan";
		break;
	case GL_RENDERER:
		r = "mini 2D libgl(glx) compatibility profile for the 32bits part of the steam runtime";
		break;
	case GL_VERSION:
		/* r = "4.6"; */
		r = "1.5";
		break;
	case GL_EXTENSIONS:
		r = ""; /* only in compatibility profile */
		break;
	case GL_SHADING_LANGUAGE_VERSION:
		/* r = "4.60"; */
		r = 0;
		break;
	default:
		r = 0;
		break;
	}
leave:
	LEAVE;
	return r;
}/*}}}*/
/*{{{*/void glDeleteTextures(GLsizei n, const GLuint *textures)
{
	struct thd_data_t *self_data;
	struct ctx_t *ctx;
	GLsizei i;

	ONCE_AND_ENTER;
	TRACE("n=%d:textures=%p", n, textures);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;	
	}
	ctx = self_data->ctx;
	i = 0;
	loop {
		if (i == n)
			break;
		/* the name of a texture - 1 is its index in texs array */
		if ((textures[i] != 0) && ((textures[i] - 1) < ctx->texs.n)) {
			struct tex_t *tex;

			TRACE("ctx=%p:deleting texture 0x%x", ctx, textures[i]);
			ctx->texs.texture2d = 0; /* reset to default */
			tex = &self_data->ctx->texs.a[textures[i] - 1];
			tex->bound = false;
			free(tex->pixels);
			tex->pixels = 0;
			tex->width = 0;
			tex->height = 0;
			tex->format = 0;
			tex->write_to_client.align = 4;
			tex->read_from_client.align = 4;
			tex->read_from_client.row_pixels_n = 0;
		}
		++i;
	}
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glGenTextures(GLsizei n, GLuint *textures)
{
	struct thd_data_t *self_data;
	struct ctx_t *ctx;
	struct tex_t *texs;
	GLsizei texs_n;
	GLsizei i; 
	GLsizei j;
	GLsizei reused_n;

	ONCE_AND_ENTER;
	TRACE("n=%d:textures=%p", n, textures);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;
	}
	if (n == 0)
		goto leave;
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;	
	}
	ctx = self_data->ctx;
	texs = ctx->texs.a;
	texs_n = ctx->texs.n;
	i = 0;
	j = 0;
	loop { /* reuse as many as we can */
		if (j == texs_n)
			break;
		if (!texs[j].bound) {
			textures[i] = j + 1; /* the name is the index + 1 */
			TRACE("ctx=%p:name=0x%x", ctx, textures[i]);
			++i;
			if (i == n)
				goto leave;
		}
		++j;
	}
	/* brand new ones */
	reused_n = i;
	ctx->texs.a = realloc(ctx->texs.a, sizeof(*(ctx->texs.a)) * (texs_n
							+ (n - reused_n)));
	texs = ctx->texs.a;
	j = texs_n;
	loop {
		if (j == (texs_n + (n - reused_n)))
			break;
		texs[j].bound = false;
		texs[j].target = 0;
		texs[j].pixels = 0;
		texs[j].width = 0;
		texs[j].height = 0;
		texs[j].format = 0;
		texs[j].write_to_client.align = 4;
		texs[j].read_from_client.align = 4;
		texs[j].read_from_client.row_pixels_n = 0;
		textures[i] = j + 1; /* the name is the index + 1 */
		TRACE("ctx=%p:name=0x%x", self_data->ctx, textures[i]);
		++j;
		++i;
	}
	ctx->texs.n += n - reused_n;
leave:
	LEAVE;
}/*}}}*/
/*{{{void glBindTexture(GLenum target, GLuint texture)*/
/*
 * The type of a texture is defined by the target while being bound for the
 * first time. Then the type cannot be changed and the texture can be re-bound
 * only to the same target (or compatible?).
 * Texture names are local to the current context.
 */
void  glBindTexture(GLenum target, GLuint texture)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("target=0x%x(%s):texture=0x%x", target, tex_target_str(target), texture);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;	
	}
	/* the texture name - 1 is the index in the texs array */
	if ((texture - 1) > self_data->ctx->texs.n) {
		TRACE("WARNING:ctx=%p:texture name 0x%x was not generated", self_data->ctx, texture);
		goto leave;
	}
	/* here, the texture name was already instanciated */
	if (self_data->ctx->texs.a[texture - 1].bound) {
		if (self_data->ctx->texs.a[texture - 1].target != target) {
			LOG("ERROR:ctx=%p:texture 0x%x was already bound to target 0x%x(%s) which is different to the new target 0x%x(%s), ignoring", self_data->ctx, texture, self_data->ctx->texs.a[texture - 1].target, tex_target_str(self_data->ctx->texs.a[texture - 1].target), target, tex_target_str(target));
			goto leave;
		} else
			TRACE("ctx=%p:rebinding texture 0x%x to same target 0x%x(%s)", self_data->ctx, texture, target, tex_target_str(target));
	} else
		TRACE("ctx=%p:binding texture 0x%x to target 0x%x(%s)", self_data->ctx, texture, target, tex_target_str(target));
	self_data->ctx->texs.a[texture - 1].bound = true;
	self_data->ctx->texs.a[texture - 1].target = target;
	if (target == GL_TEXTURE_2D)
		self_data->ctx->texs.texture2d = texture;
leave:
	LEAVE;
}/*}}}*/
/*{{{void glTexImage2D(GLenum target, GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid *pixels)*/
/*
 * It will load a texture target of the active texture unit from some data.
 * The data is decribed by format/type parameters, and the loading process
 * will select/convert from this data to the internal format.
 * The data pointer can be 0, they only texture memory allocation is done.
 */
void  glTexImage2D(GLenum target, GLint level,
                                    GLint internalFormat,
                                    GLsizei width, GLsizei height,
                                    GLint border, GLenum format, GLenum type,
                                    const GLvoid *pixels)
{
	struct thd_data_t *self_data;
	Bool ret;
	GC gc;
	struct ctx_t *ctx;
	struct tex_t *tex;
	GLuint texture2d;
	size_t tex_bytes_n;
#ifdef AVX2_32BITS
	struct {
		void *dst;
		u32 dst_x_offset;
		u32 dst_y_offset;
		u32 dst_width_pixs_n;
		u32 dst_line_pixs_n;
		void *src;
		u32 src_line_pixs_n;
		u32 height_lines_n;
	} avx2_input;
#endif

	ONCE_AND_ENTER;
	TRACE("target=0x%x(%s):level=%d:internalFormat=0x%x(%s):width=%d:height=%d:border=%d:format=0x%x(%s):type=0x%x(%s):pixels=%p", target, tex_target_str(target), level, internalFormat, tex_format_str(internalFormat), width, height, border, format, tex_format_str(format), type, pixel_data_format_str(type), pixels);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;	
	}
	ctx = self_data->ctx;
	texture2d = ctx->texs.texture2d;
	tex = &ctx->texs.a[texture2d - 1];
	if (format != GL_BGRA && format != GL_RGBA) {
		LOG("ERROR:ctx=%p:texture=0x%x:we don't support texture format 0x%x(%s)", ctx, texture2d, format, tex_format_str(format));
		goto leave;
	}
	/* for the above pixel formats, alignment of 1 of 4 are the same */
	if (tex->read_from_client.align != 1
					&& tex->read_from_client.align != 4) {
		LOG("ERROR:ctx=%p:texture=0x%x:we don't support reading from client memory with an alignement of %u bytes", ctx, texture2d, tex->read_from_client.align);
		goto leave;
	}
	/*
	 * there may be no data, then only allocation occurs, and loading
	 * would happen using subtexture loads
	 */
	if (tex->pixels != 0) {/* rebinding? */
		TRACE("WARNING:ctx=%p:texture=0x%x:have data already, freeing", ctx, texture2d);
		free(tex->pixels);
		tex->pixels = 0;
	}
	/* here, format is GL_BGRA or GL_RGBA with an alignment of 1 */
	tex_bytes_n = width * height * 4;
	tex->pixels = malloc(tex_bytes_n);
	
	if (pixels != 0) {
#ifdef AVX2_32BITS
		avx2_input.dst = tex->pixels;
		avx2_input.dst_x_offset = 0;
		avx2_input.dst_y_offset = 0;
		avx2_input.dst_width_pixs_n = width;
		avx2_input.dst_line_pixs_n = width;
		avx2_input.src = (void*)pixels;
		if (tex->read_from_client.row_pixels_n == 0)
			avx2_input.src_line_pixs_n = width;
		else
			avx2_input.src_line_pixs_n = tex->read_from_client.row_pixels_n;
		avx2_input.height_lines_n = height;
		glTexXImage2D_avx2(&avx2_input);
#else
		if (tex->read_from_client.row_pixels_n == 0)
			memcpy(tex->pixels, pixels, tex_bytes_n);
		else {
			GLsizei x;
			GLsizei y;

			y = 0;
			loop {
				if (y == height)
					break;
				x = 0;
				loop {
					u8 *psrc;
					u8 *pdst;

					if (x == width)
						break;
					psrc = (u8*)pixels;
					psrc += (y * tex->read_from_client.row_pixels_n + x) << 2;
					pdst = tex->pixels + ((y * width + x) << 2);
					memcpy(pdst, psrc, 4);
					++x;
				}
				++y;
			}
		}
#endif
	}
	tex->width = width;
	tex->height = height;
	tex->format = format;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *pixels)
{
	struct thd_data_t *self_data;
	struct ctx_t *ctx;
	GLuint texture2d;
	struct tex_t *tex;
#ifdef AVX2_32BITS
	struct {
		void *dst;
		u32 dst_x_offset;
		u32 dst_y_offset;
		u32 dst_width_pixs_n;
		u32 dst_line_pixs_n;
		void *src;
		u32 src_line_pixs_n;
		u32 height_lines_n;
	} avx2_input;
#else
	GLuint dst_line_bytes_n;
	u8 *src_pixels;
	u8 *dst_pixels;
	GLuint src_x;
	GLuint src_y;
	GLuint dst_x;
	GLuint dst_y;
#endif

	ONCE_AND_ENTER;
	TRACE("target=0x%x(%s):level=%d:xoffset=%d:yoffset=%d:width=%d:height=%d:format=0x%x(%s):type=0x%x(%s):pixels=%p", target, tex_target_str(target), level, xoffset, yoffset, width, height, format, tex_format_str(format), type, pixel_data_format_str(type), pixels);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	if (pixels == 0)
		goto leave;
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;	
	}
	ctx = self_data->ctx;
	texture2d = ctx->texs.texture2d;
	tex = &ctx->texs.a[texture2d - 1];
	if (format != tex->format) {
		LOG("ERROR:ctx=%p:texture=0x%x:the texture has format 0x%x(%s) but the update data has format 0x%x(%s)", ctx, texture2d - 1, tex->format, tex_format_str(tex->format), format, tex_format_str(format));
		goto leave;
	}
	if (format != GL_BGRA && format != GL_RGBA) {
		LOG("ERROR:ctx=%p:texture=0x%x:we don't support texture format 0x%x(%s)", ctx, texture2d - 1, format, tex_format_str(format));
		goto leave;
	}
	/* for the above pixel formats, alignment of 1 of 4 are the same */
	if (tex->read_from_client.align != 1
					&& tex->read_from_client.align != 4) {
		LOG("ERROR:ctx=%p:texture=0x%x:we don't support reading from client memory with an alignement of %u bytes", ctx, texture2d - 1, tex->read_from_client.align);
		goto leave;
	}
	if (pixels == 0 || tex->pixels == 0 || width == 0 || height == 0)
		goto leave;
#ifdef AVX2_32BITS
	avx2_input.dst = tex->pixels;
	avx2_input.dst_x_offset = xoffset;
	avx2_input.dst_y_offset = yoffset;
	avx2_input.dst_width_pixs_n = width;
	avx2_input.dst_line_pixs_n = tex->width;
	avx2_input.src = (void*)pixels;
	if (tex->read_from_client.row_pixels_n == 0)
		avx2_input.src_line_pixs_n = width;
	else
		avx2_input.src_line_pixs_n = tex->read_from_client.row_pixels_n;
	avx2_input.height_lines_n = height;
	glTexXImage2D_avx2(&avx2_input);
#else
	src_pixels = (u8*)pixels;
	dst_pixels = tex->pixels;
	if (pixels == 0 || tex->pixels == 0 || width == 0 || height == 0)
		goto leave;
	dst_line_bytes_n = tex->width << 2;
	dst_y = yoffset;
	src_y = 0;
	TRACE("texture2d=0x%x dst_line_bytes_n=%u", texture2d, dst_line_bytes_n);
	loop {
		if (dst_y == yoffset + height)
			break;
		dst_x = xoffset;
		src_x = 0;
		loop {
			u8 *dst_pix;
			u8 *src_pix;

			if (dst_x == xoffset + width)
				break;
			dst_pix = dst_pixels + dst_y * dst_line_bytes_n
								+ (dst_x << 2);
			if (tex->read_from_client.row_pixels_n == 0)
				src_pix = src_pixels + ((src_y * width + src_x) << 2);
			else
				src_pix = src_pixels + ((src_y * tex->read_from_client.row_pixels_n + src_x) << 2);
			memcpy(dst_pix, src_pix, 4);
			++dst_x;
			++src_x;
		}	
		++dst_y;
		++src_y;
	}
#endif
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glEnable(GLenum cap)
{
	struct thd_data_t *self_data;
	struct ctx_t *ctx;

	ONCE_AND_ENTER;
	TRACE("cap=0x%x(%s)", cap, disable_enable_cap_str(cap));
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	ctx = self_data->ctx;
	if (cap == GL_BLEND)
		ctx->blending_enabled = true;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glMatrixMode(GLenum mode)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("mode=0x%x(%s)", mode, matrixmode_mode_str(mode));
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	self_data->ctx->mtxs.current = mode;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glLoadIdentity(void)
{
	struct thd_data_t *self_data;
	double *mtx;
	unsigned int i;
	unsigned int j;

	ONCE_AND_ENTER;
	TRACE("void");
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	switch(self_data->ctx->mtxs.current) {
	case GL_MODELVIEW:
		mtx = self_data->ctx->mtxs.mv;
		break;
	case GL_PROJECTION:
		mtx = self_data->ctx->mtxs.proj;
		break;
	case GL_TEXTURE:
		mtx = self_data->ctx->mtxs.tex;
		break;
	case GL_COLOR:
		mtx = self_data->ctx->mtxs.col;
		break;
	default:
		LOG("WARNING:unknown matrix 0x%x in context %p", self_data->ctx->mtxs.current, self_data->ctx);
		goto leave;
	}
	i = 0;
	loop  {
		if (i == 4)
			break;
		j = 0;
		loop {
			if (j == 4)
				break;
			if (j != i)
				mtx[i * 4 + j] = 0.;
			else
				mtx[i * 4 + j] = 1.;
			++j;
		}
		++i;
	}
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glOrtho(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble near_val, GLdouble far_val)
{
	struct thd_data_t *self_data;
	struct ctx_t *ctx;

	ONCE_AND_ENTER;
	TRACE("left=%f:right=%f:bottom=%f:top=%f:near=%f:far=%f", left, right, bottom, top, near_val, far_val);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	ctx = self_data->ctx;
	if (ctx->mtxs.current != GL_PROJECTION) {
		LOG("WARNING:orthogonal project matrix applied to not projection matrix, not recording");
		goto leave;
	}
	ctx->mtxs.proj_ortho.left = left;
	ctx->mtxs.proj_ortho.right = right;
	ctx->mtxs.proj_ortho.bottom = bottom;
	ctx->mtxs.proj_ortho.top = top;
	ctx->mtxs.proj_ortho.near = near_val;
	ctx->mtxs.proj_ortho.far = far_val;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glTranslatef(GLfloat x, GLfloat y, GLfloat z)
{
	struct thd_data_t *self_data;
	struct ctx_t *ctx;

	ONCE_AND_ENTER;
	TRACE("x=%f:y=%f:z=%f", x, y, z);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	ctx = self_data->ctx;
	if (ctx->mtxs.current != GL_MODELVIEW) {
		LOG("WARNING:translation matrix applied to not model view matrix, not recording");
		goto leave;
	}
	ctx->mtxs.mv_translate.x = x;
	ctx->mtxs.mv_translate.y = y;
	ctx->mtxs.mv_translate.z = z;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glDisableClientState(GLenum cap)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("cap=0x%x(%s)", cap, disable_enable_cap_str(cap));
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* yaya */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glTexEnvf(GLenum target, GLenum pname, GLfloat param)
{
	struct thd_data_t *self_data;
	struct ctx_t *ctx;

	ONCE_AND_ENTER;
	TRACE("target=0x%x(%s):pname=0x%x(%s):param=%f(%s)", target, texenv_target_str(target), pname, texenv_pname_str(pname), param, texenv_param_str(pname, param));
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	ctx = self_data->ctx;
	if (pname == GL_TEXTURE_ENV_MODE && param != (GLfloat)GL_MODULATE)
		LOG("ctx=%p:ERROR:we don't support anything else than GL_MODULATE", ctx);
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glColor4f(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("red=%f:green=%f:blue=%f:alpha=%f", red, green, blue, alpha);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	self_data->ctx->color.red = red;
	self_data->ctx->color.green = green;
	self_data->ctx->color.blue = blue;
	self_data->ctx->color.alpha = alpha;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glReadBuffer(GLenum mode)
{
	ONCE_AND_ENTER;
	TRACE("mode=%d", mode);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	// XXX: does forget the texture alignment
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glPixelStorei(GLenum pname, GLint param)
{
	struct thd_data_t *self_data;
	struct ctx_t *ctx;

	ONCE_AND_ENTER;
	TRACE("pname=0x%x(%s):param=0x%x", pname, pixelstore_pname_str(pname), param);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	ctx = self_data->ctx;
	switch(pname) {
	case GL_UNPACK_ALIGNMENT:
		if (ctx->texs.texture2d != 0) {
			struct tex_t *tex;

			tex = &ctx->texs.a[ctx->texs.texture2d - 1];
			tex->read_from_client.align = param;
			TRACE("ctx=%p:texture2d bound texture name 0x%x, setting read from client alignment to %u bytes", ctx, ctx->texs.texture2d - 1, param);
		}
		break;
	case GL_UNPACK_ROW_LENGTH:
		if (ctx->texs.texture2d != 0) {
			struct tex_t *tex;

			tex = &ctx->texs.a[ctx->texs.texture2d - 1];
			tex->read_from_client.row_pixels_n = param;
			TRACE("ctx=%p:texture2d bound texture name 0x%x, setting read from client row lenght to %u pixels", ctx, ctx->texs.texture2d - 1, param);
		}
		break;
	case GL_PACK_ALIGNMENT:
		if (ctx->texs.texture2d != 0) {
			struct tex_t *tex;

			tex = &ctx->texs.a[ctx->texs.texture2d - 1];
			tex->write_to_client.align = param;
			TRACE("ctx=%p:texture2d bound texture name 0x%x, setting write to client alignment to %u bytes", ctx, ctx->texs.texture2d - 1, param);
		}
		break;
	default:
		TRACE("WARNING:unhandled storage parameter");
		break;
	}
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glTexParameteri(GLenum target, GLenum pname, GLint param)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("target=0x%x(%s):pname=0x%x:param=0x%x", target, tex_target_str(target), pname, param);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glReadPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid *pixels)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("x=%d:y=%d:width=%d:height=%d:format=%d:type=%d:pixels=%p", x, y, width, height, format, type, pixels);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glViewport(GLint x, GLint y, GLsizei width, GLsizei height)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("x=%d:y=%d:width=%d:height=%d", x, y, width, height);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();	
	if (self_data->ctx == 0) {
		LOG("WARNING:the self thread has no current context, ignoring and continuing");
		goto leave;
	}
	self_data->ctx->vp.x = x;
	self_data->ctx->vp.y = y;
	self_data->ctx->vp.width = width;
	self_data->ctx->vp.height = height;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glBegin(GLenum mode)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("mode=0x%x(%s)", mode, begin_mode(mode));
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;	
	}
	memset(&self_data->ctx->begin_end_blk.quad, 0,
				sizeof(self_data->ctx->begin_end_blk.quad));
	self_data->ctx->begin_end_blk.recording = true;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glEnd( void )
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("void");
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;	
	}
	render_in_shm(self_data->ctx);
	self_data->ctx->begin_end_blk.recording = false;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glTexCoord2f( GLfloat s, GLfloat t )
{
	struct thd_data_t *self_data;
	struct ctx_t *ctx;

	ONCE_AND_ENTER;
	TRACE("s=%f:t=%f", s, t);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;	
	}
	ctx = self_data->ctx;
	if (!ctx->begin_end_blk.recording) {
		LOG("WARNING:outside of a glBegin/glEnd block, ignoring");
		goto leave;
	}
	if (ctx->begin_end_blk.quad.i > 4) {
		LOG("WARNING:out of storage, ignoring");
		goto leave;
	}
	ctx->begin_end_blk.quad.vs[ctx->begin_end_blk.quad.i].tex.s = s;
	ctx->begin_end_blk.quad.vs[ctx->begin_end_blk.quad.i].tex.t = t;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glVertex2f(GLfloat x, GLfloat y)
{
	struct thd_data_t *self_data;
	struct ctx_t *ctx;

	ONCE_AND_ENTER;
	TRACE("x=%f:y=%f", x, y);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;	
	}
	ctx = self_data->ctx;
	if (!ctx->begin_end_blk.recording) {
		LOG("WARNING:outside of a glBegin/glEnd block, ignoring");
		goto leave;
	}
	if (ctx->begin_end_blk.quad.i > 4) {
		LOG("WARNING:out of storage, ignoring");
		goto leave;
	}
	ctx->begin_end_blk.quad.vs[ctx->begin_end_blk.quad.i].plane.x = x;
	ctx->begin_end_blk.quad.vs[ctx->begin_end_blk.quad.i].plane.y = y;
	ctx->begin_end_blk.quad.vs[ctx->begin_end_blk.quad.i].color.red = 
								ctx->color.red;
	ctx->begin_end_blk.quad.vs[ctx->begin_end_blk.quad.i].color.green = 
							ctx->color.green;
	ctx->begin_end_blk.quad.vs[ctx->begin_end_blk.quad.i].color.blue = 
								ctx->color.blue;
	ctx->begin_end_blk.quad.vs[ctx->begin_end_blk.quad.i].color.alpha = 
							ctx->color.alpha;
	++(ctx->begin_end_blk.quad.i); /* vertex coords, then advance */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glColor4ub(GLubyte red, GLubyte green, GLubyte blue, GLubyte alpha)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("red=0x%02x:green=0x%02x:blue=0x%02x:alpha=0x%02x", red, green, blue, alpha);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	self_data->ctx->color.red = (float)red / 255.;
	self_data->ctx->color.green = (float)green / 255.;
	self_data->ctx->color.blue = (float)blue / 255.;
	self_data->ctx->color.alpha = (float)alpha / 255.;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glClearColor(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("red=%f:green=%f:blue=%f:alpha=%f", red, green, blue, alpha);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	self_data->ctx->clear_color.red = red;
	self_data->ctx->clear_color.green = green;
	self_data->ctx->clear_color.blue = blue;
	self_data->ctx->clear_color.alpha = alpha;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glClear(GLbitfield mask)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("mask=0x%x", mask);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;	
	}
	if ((mask & GL_COLOR_BUFFER_BIT) != 0)
		color_buffer_clear(self_data->ctx);
leave:
	LEAVE;
}/*}}}*/
/*{{{*/GLenum glGetError(void)
{
	int r;

	ONCE_AND_ENTER;
	TRACE("void");
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = 0;
		goto leave;		
	}
	r = 0;
leave:
	LEAVE;
	return 0;
}/*}}}*/
/*{{{*/void glGetIntegerv(GLenum pname, GLint *params)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("pname=0x%x(%s):params=%p", pname, getinteger_pname_str(pname), params);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	if (params == 0)
		goto leave;
	switch(pname) {
	case GL_VIEWPORT:
		params[0] = self_data->ctx->vp.x;
		params[1] = self_data->ctx->vp.y;
		params[2] = self_data->ctx->vp.width;
		params[3] = self_data->ctx->vp.height;
		TRACE("viewport:x=%d:y=%d:width=%d:height=%d", params[0], params[1], params[2], params[3]);
		break;
	case GL_NUM_EXTENSIONS:
		params[0] = 0; /* no extensions */
		break;
	default:
		break;
	}
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glBlendFunc(GLenum sfactor, GLenum dfactor)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("sfactor=0x%x(%s):dfactor=0x%x(%s)", sfactor, blendfunc_factor_str(sfactor), dfactor, blendfunc_factor_str(dfactor));
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	if (sfactor != GL_SRC_ALPHA || dfactor != GL_ONE_MINUS_SRC_ALPHA)
		LOG("ctx=%p:ERROR: blendind function not supported", self_data->ctx);
leave:
	LEAVE;
}/*}}}*/
/*{{{*/GLint glRenderMode(GLenum mode)
{
	int r;
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("mode=0x%x(%s)", mode, rendermode_str(mode));
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = 0;
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		r = 0;
		goto leave;
	}
	r = 0;
leave:
	LEAVE;
	return r;
}/*}}}*/
/*{{{*/void glDisable(GLenum cap)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("cap=0x%x(%s)", cap, disable_enable_cap_str(cap));
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* yayayaya */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glShadeModel(GLenum mode)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("mode=0x%x(%s)", mode, shademodel_mode_str(mode));
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* yayayaya */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/GLboolean glIsTexture(GLuint texture)
{
	/* XXX: a texture has a name AND is bound */
	struct thd_data_t *self_data;
	GLboolean r;

	ONCE_AND_ENTER;
	TRACE("name=0x%x", texture);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		r = GL_FALSE;
		goto leave;
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		r = GL_FALSE;
		goto leave;
	}
	if (texture == 0) {
		r = GL_FALSE;
		goto leave;
	}
	/* texture - 1 is the index in the texs array */
	if ((texture - 1) >= self_data->ctx->texs.n) {
		/* this name was not instanciated */
		r = GL_FALSE;
		goto leave;
	}
	if (self_data->ctx->texs.a[texture - 1].bound) {
		r = GL_TRUE;
		goto leave;
	}
	r = GL_FALSE;
leave:
	TRACE("ctx=%p:0x%x is %sa texture", self_data->ctx, texture, (r == GL_FALSE) ? "not " : "");
	LEAVE;
	return r;
}/*}}}*/
/*{{{*/void glTexParameterf(GLenum target, GLenum pname, GLfloat param)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("target=0x%x(%s):pname=0x%x(%s):param=%f(%s)", target, tex_target_str(target), pname, texparameter_pname_str(pname), param, texparameterf_param_str(pname, param));
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glColor4fv(const GLfloat *v)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
#ifdef TRACE_ON
	if (v != 0) {
		TRACE("v=%p:red=%f:green=%f:blue=%f:alpha=%f", v, v[0], v[1], v[2], v[3]);
	} else {
		TRACE("v=0x%p",v);
	}
#endif
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	self_data->ctx->color.red = v[0];
	self_data->ctx->color.green = v[1];
	self_data->ctx->color.blue = v[2];
	self_data->ctx->color.alpha = v[3];
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glLineWidth(GLfloat width)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("width=%f", width);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/GLboolean glIsEnabled(GLenum cap)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("cap=0x%x(%s)", cap, disable_enable_cap_str(cap));
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
	return GL_FALSE;
}/*}}}*/
/*{{{*/void GLAPIENTRY glGetBooleanv(GLenum pname, GLboolean *params)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("cap=0x%x", pname);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
	*params = GL_FALSE;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glColorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("red=%d green=%d blue=%d alpha=%d", red, green, blue, alpha);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void GLAPIENTRY glDrawArrays(GLenum mode, GLint first, GLsizei count)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("mode=0x%x first=%d count=%d", mode, first, count);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glTexCoord2i(GLint s, GLint t)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("s=%d t=%d", s, t);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glDrawElements(GLenum mode, GLsizei count, GLenum type,
							const GLvoid *indices)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("mode=0x%x count=%d type=0x%x indices=%p", mode, count, type,
								indices);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glTexCoord2d(GLdouble s, GLdouble t)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("s=%f t=%f", s, t);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glVertex3f(GLfloat x, GLfloat y, GLfloat z)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("x=%f y=%f z=%f", x, y, z);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glDepthRange(GLclampd near_val, GLclampd far_val)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("near_val=%f far_val=%f", near_val, far_val);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glDepthMask(GLboolean flag)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("flag=%u", flag);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glPolygonMode(GLenum face, GLenum mode)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("face=0x%x mode=0x%x", face, mode);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glPopClientAttrib(void)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("void");
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glPopAttrib(void)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("void");
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glLoadMatrixf(const GLfloat *m)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("m=%p", m);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glPushAttrib(GLbitfield mask)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("masx=0x%x", mask);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glPushClientAttrib(GLbitfield mask)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("masx=0x%x", mask);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glPixelZoom(GLfloat xfactor, GLfloat yfactor)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("xfactor=%f yfactor=%f", xfactor, yfactor);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glGetDoublev(GLenum pname, GLdouble *params)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("pname=0x%x params=%p", pname, params);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
	*params = 0.;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glGetFloatv(GLenum pname, GLfloat *params)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("pname=0x%x params=%p", pname, params);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
	/* muh? */
	*params = 0.;
leave:
	LEAVE;
}/*}}}*/
/*{{{*/void glScalef(GLfloat x, GLfloat y, GLfloat z)
{
	struct thd_data_t *self_data;

	ONCE_AND_ENTER;
	TRACE("x=%f y=%f z=%f", x, y, z);
	if (!glx_extension_enabled) {
		TRACE("glx extension not initialized");
		goto leave;		
	}
	self_data = self_data_get();
	if (self_data->ctx == 0) {
		TRACE("the calling thread has no current context");
		goto leave;
	}
leave:
	LEAVE;
}/*}}}*/
#endif
