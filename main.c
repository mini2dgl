#define _GNU_SOURCE
#include <stdbool.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <GL/internal/glx.h>
#include <X11/extensions/XShm.h>
/*----------------------------------------------------------------------------*/
#undef TRACE_ON
#define REDIRECT_LOG_TO_TRACE_FILE
/*----------------------------------------------------------------------------*/
#define u8 unsigned char
#define u16 unsigned short
#define u32 unsigned int
#define s32 int
#define loop for(;;)
/*----------------------------------------------------------------------------*/
#include "log_trace.c"
/*----------------------------------------------------------------------------*/
static pthread_once_t prolog_once = PTHREAD_ONCE_INIT;
static pthread_mutex_t global_mutex;
static pthread_key_t thd_data_key;
static void thd_data_cleanup(void *key_value);
/*----------------------------------------------------------------------------*/
/* very limited, but should be more than enough */
static bool glx_extension_enabled = false;
/*----------------------------------------------------------------------------*/
#ifdef AVX2_32BITS
extern void glTexXImage2D_avx2(void* input);
extern void clearcolor_avx2(void* input);
extern void minmax_avx2(void* ctx);
extern void alphablend_rgba_avx2(void* input);
#endif
/*----------------------------------------------------------------------------*/
/*{{{ ipc tracker */
/* stale ipc... wonder why this is still a thing */
static struct ipcs_tracker {
	struct {
		int id;
		void *mem;
	} *slots;
	unsigned int n;
} ipcs_tracker;
static void ipc_track(int id, void *mem)
{
	unsigned int i;
	/* try to get an existing slot */
	i = 0;
	loop {
		if (i == ipcs_tracker.n || ipcs_tracker.slots[i].id == -1)
			break;
		++i;
	}
	if (i == ipcs_tracker.n) {
		ipcs_tracker.slots = realloc(ipcs_tracker.slots,
					sizeof(*ipcs_tracker.slots) * (i + 1));
		++(ipcs_tracker.n);
	}
	ipcs_tracker.slots[i].id = id;
	ipcs_tracker.slots[i].mem = mem;
	TRACE("tracking id=%d mem=%p", ipcs_tracker.slots[i].id, ipcs_tracker.slots[i].mem);
}
static void ipc_untrack(int id)
{
	unsigned int i;

	i = 0;
	loop {
		if (i == ipcs_tracker.n)
			return;
		if (ipcs_tracker.slots[i].id == id)
			break;
		++i;
	}
	TRACE("untracking id=%d mem=%p", ipcs_tracker.slots[i].id, ipcs_tracker.slots[i].mem);
	ipcs_tracker.slots[i].id = -1;
}
static void ipcs_rm(void)
{
	unsigned int i;

	i = 0;
	loop {
		if (i == ipcs_tracker.n)
			return;
		TRACE("slot[%u]->id=%d:mem=%p", i, ipcs_tracker.slots[i].id, ipcs_tracker.slots[i].mem);
		if (ipcs_tracker.slots[i].id != -1) {
			TRACE("removing id=%d mem=%p", ipcs_tracker.slots[i].id, ipcs_tracker.slots[i].mem);
			shmdt(ipcs_tracker.slots[i].mem);
			shmctl(ipcs_tracker.slots[i].id, IPC_RMID, 0);
		}
		++i;
	}
}
static void ipcs_tracker_init(void)
{
	int r;

	ipcs_tracker.slots = 0;
	ipcs_tracker.n = 0;
	r = atexit(ipcs_rm);
	if (r != 0)
		LOG("ERROR:unable to register IPC cleanup handler, your system will have stale IPCs on abnormal termination");
	TRACE("tracker of IPCs installed");
}
/*}}}*/
/*{{{ global lock */
#define LOCK lock((void*)__func__)
#define UNLOCK unlock((void*)__func__)
static void lock(void *fn)
{
	int r;

	r = pthread_mutex_lock(&global_mutex);
	if (r != 0) {
		LOG("ERROR:from %s:unable to lock the global mutex", fn);
		abort();
	}
}
static void unlock(void *fn)
{
	int r;

	r = pthread_mutex_unlock(&global_mutex);
	if (r != 0) {
		LOG("ERROR:from %s:unable to unlock the global mutex", fn);
		abort();
	}
}
/*}}}*/
/*{{{ drawable tracker and listener */
/*
 * XXX: it is not clear how a drawable is "removed" from glx/gl, then this array
 * will grow infinitely
 */
struct drawable_t {
	int id; /* -1 means available slot */
	int bytes_per_line;
	int height;
	int width;
	int depth;
	Visual *visual;
	int screen;
	struct {
		int id; /* if -1, no shm segment attached */
		u8 *m;
	} shm;
};
static struct {
	struct drawable_t *slots;
	int n;
} ds;
static void ds_init(void)
{
	ds.slots = 0;
	ds.n = 0;
}
static struct drawable_t *ds_get(int id)
{
	int d;

	d = 0;
	loop {
		if (ds.n == d)
			return 0;
		if (ds.slots[d].id == id)
			return &ds.slots[d];
		++d;
	}
}
/* must be called with the global mutex locked */
static void ds_destroy(int slot)
{
	if (ds.slots[slot].shm.id != -1) {
		shmdt(ds.slots[slot].shm.m);
		shmctl(ds.slots[slot].shm.id, IPC_RMID, 0);
		ipc_untrack(ds.slots[slot].shm.id);
		ds.slots[slot].shm.id = -1;
	}
	ds.slots[slot].id = -1;
}
struct drawable_destroy_listener_arg {
	Display *dpy;
	int slot;
};
static void *drawable_destroy_listener(void *arg)
{
	struct drawable_destroy_listener_arg *a;

	a = arg;
	loop {
		XEvent e;

		memset(&e, 0, sizeof(e));
		XNextEvent(a->dpy, &e);
		if (e.type == DestroyNotify) {
			int s;

			LOCK;
			if (e.xdestroywindow.window == ds.slots[a->slot].id) {
				TRACE("destroying drawable 0x%x", ds.slots[a->slot].id);
				XCloseDisplay(a->dpy);
				ds_destroy(a->slot);
				free(a);
				UNLOCK;
				pthread_exit(0);
			}
			UNLOCK;
		}
	}
}
static void drawable_destroy_listener_spawn(Display *dpy, int slot)
{
	Display *local_dpy;
	XSetWindowAttributes attrs;
	int r;
	struct drawable_destroy_listener_arg *arg;
	pthread_t listener;

	local_dpy = XOpenDisplay(DisplayString(dpy));
	if (local_dpy == 0) {
		LOG("ERROR:unable to get a local display connexion for drawable 0x%x from display %p, will leak ipc shared memory segment, will probably leak ipc shared memory segments", ds.slots[slot].id, dpy);
		return;
	}
	/* the event mask is specific to our local dpy */
	memset(&attrs, 0, sizeof(attrs));
	attrs.event_mask = StructureNotifyMask;
	XChangeWindowAttributes(local_dpy, ds.slots[slot].id, CWEventMask,
									&attrs);
	arg = malloc(sizeof(*arg));
	arg->dpy = local_dpy;
	arg->slot = slot;
	TRACE("spawing destroy listener thread for drawable 0x%x", ds.slots[slot].id);
	r = pthread_create(&listener, 0, drawable_destroy_listener, arg);
	if (r != 0) {
		LOG("ERROR:unable to spawn the destroy listener for drawable 0x%x, will probably leak the ipc shared memory segments", ds.slots[slot].id);
		free(arg);
		return;
	}
	TRACE("listener thread for drawable 0x%x is 0x%x", ds.slots[slot].id, listener);
}
static struct drawable_t *ds_new(Display *dpy, int id, int bytes_per_line,
		int height, int width, int depth, Visual *visual, int screen)
{
	struct drawable_t *new;
	int s;

	s = 0;
	loop {
		if (s == ds.n) {
			ds.slots = realloc(ds.slots, sizeof(*ds.slots)
								* (ds.n + 1));
			ds.slots[s].id = -1;
			++(ds.n);
			break;
		}
		if (ds.slots[s].id == -1)
			break;
		++s;
	}
	new = &ds.slots[s];
	new->shm.id = shmget(IPC_PRIVATE, bytes_per_line * height,
								IPC_CREAT|0777);
	if (new->shm.id == -1) {
		LOG("ERROR:unable to create a new ipc shared memory segment for drawable 0x%x", id);
		return 0;
	}
	new->shm.m = shmat(new->shm.id, 0, 0);
	if (new->shm.m == 0) {
		shmctl(new->shm.id, IPC_RMID, 0);
		new->shm.id = -1;
		return 0;
	}
	ipc_track(new->shm.id, new->shm.m);
	new->bytes_per_line = bytes_per_line;
	new->height = height;
	new->width = width;
	new->depth = depth;
	new->visual = visual;
	new->screen = screen;
	new->id = id;
	drawable_destroy_listener_spawn(dpy, s);
	return new;
}
/*}}}*/
/*{{{ init/fini entry/leave  */
static void init(void)
{
	int r;

	trace_init();
	r = pthread_mutex_init(&global_mutex, 0);
	if (r != 0) {
		LOG("ERROR:unable to init the global mutex");
		abort();
	} else 
		LOG("global mutex initialized");
	r = pthread_key_create(&thd_data_key, thd_data_cleanup);
	if (r != 0) {
		LOG("ERROR:unable to create the thread data key");
		abort();
	} else
		TRACE("thread data key created");
	glx_extension_enabled = false;
	ipcs_tracker_init();
	ds_init();
}
static void enter(void *fn, int *old_cancel_state)
{
	int r;

	/* XXX: we hijack -1 as "failed" to set on entry */
	*old_cancel_state = -1;
	r = pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, old_cancel_state);
	if (r != 0)
		LOG("WARNING:from %s:unable to disable thread cancellation", fn);
	lock(fn);
}
static void leave(void *fn, int old_cancel_state)
{
	int r;

	unlock(fn);
	/* we failed to set the cancel state on entry, don't touch it */
	if (old_cancel_state == -1) 
		return;
	r = pthread_setcancelstate(old_cancel_state, 0);
	if (r != 0)
		LOG("WARNING:from %s:unable to restore client thread cancel state", fn);
}
#define ONCE_AND_ENTER \
	int old_cancel_state; \
\
	pthread_once(&prolog_once, init); \
	enter((void*)__func__, &old_cancel_state)
#define ENTER enter((void*)__func__, &old_cancel_state)
#define LEAVE leave((void*)__func__, old_cancel_state)
/*}}}*/
/*{{{ misc types */
struct tex_t {
	bool bound;
	GLenum target;
	u8 *pixels;
	GLuint width;
	GLuint height;
	GLenum format;
	struct {
		GLint align;		/* initial value is 4 */
		GLint row_pixels_n;	/* initial value is 0 */
	} read_from_client; /* we are the "server" here */
	struct {
		GLint align;	/* initial value is 4 */
	} write_to_client;
};
struct ctx_t { /* it is inited with zeros, cleanup once unused */
	bool was_already_made_current;
	struct {
		void *dpy; /* XXX: for record only, in theory, do not use */
		XVisualInfo visinf;
	} create;
	GLXDrawable drawable;
	GLXDrawable read; /* the drawable to read from */
	struct { /* set initially to the window size of the drawable */
		GLint x;
		GLint y;
		GLint width;
		GLint height;
	} vp; /* viewport */
	struct {
		unsigned int current;

		struct { /* record the last glTranslate */
			float x;
			float y;
			float z;
		} mv_translate;
		double mv[4*4];		/* model view */
		
		struct { /* record the last glOrtho */
			double left;
			double right;
			double bottom;
			double top;
			double near;
			double far;
		} proj_ortho;
		double proj[4*4];	/* projection */

		double tex[4*4];	/* texture */
		double col[4*4];	/* color */
	} mtxs; /* matrixes */
	struct {
		float red;
		float green;
		float blue;
		float alpha;
	} clear_color;
	/* defines the default vertex primary color attribute */
	struct {
		float red;
		float green;
		float blue;
		float alpha;
	} color;
	bool destroyed;
	struct {
		bool attached;
		pthread_t thd;
		Display *dpy; /* display at glXMakeCurrent */
	} current;
	/* the index in the array + 1 is actually the name of the texture */
	struct {
		struct tex_t *a;
		GLsizei n;
		/*
		 * texture name which is bound to the GL_TEXTURE_2D on the
		 * active texture unit, 0 is the default texture.
		 */
		GLuint texture2d;
	} texs;
	struct {
		bool recording;
		/* XXX: we should record also the current color attribute */
		struct {
			struct {
				struct { /* between 0.0 and 1.0 */
					/* opengl = top->bottom */
					GLfloat s;
					GLfloat t;
				} tex;
				struct { /* viewport "mapped" */
					GLfloat x;
					GLfloat y;
				} plane;
				struct {
					GLfloat red;
					GLfloat green; 
					GLfloat blue;
					GLfloat alpha;
				} color;
			} vs[4];
			int i; /* current vs for coords recording */
		} quad;
	} begin_end_blk;
	bool blending_enabled;
};
struct thd_data_t {
	struct ctx_t *ctx;
};
/*}}}*/
/*{{{ main */
static struct thd_data_t *self_data_get(void)
{
	struct thd_data_t *thd_data;

	thd_data = pthread_getspecific(thd_data_key);
	if (thd_data == 0) {
		int r;

		thd_data = calloc(1, sizeof(*thd_data));
		r = pthread_setspecific(thd_data_key, thd_data);
		if (r != 0) {
			LOG("ERROR:unable to attach self thread specific data");
			abort();
		} else {
			TRACE("%p thd_data attached to self thread", thd_data);
		}
	}
	return thd_data;
}
static void self_detach_ctx_with(Display *dpy, struct thd_data_t *self_data)
{
	pthread_t self;

	self = pthread_self();
	if (self_data->ctx == 0)
		return;
	/* sanity checks */
	if (!self_data->ctx->current.attached) {
		LOG("ERROR:the self thread current context %p is not tagged as being current to any thread, ignoring and continuing", self_data->ctx);
		abort();
	}
	/* self_data->ctx->current.attached == true */
	if (!pthread_equal(self, self_data->ctx->current.thd)) {
		LOG("ERROR:the self thread, 0x%x, current context %p has the wrong thread 0x%x", self, self_data->ctx, self_data->ctx->current.thd);
		abort();
	}
	self_data->ctx->current.thd = 0;
	self_data->ctx->current.dpy = 0;
	self_data->ctx->current.attached = false;
	self_data->ctx = 0;
}
/*
 * XXX: the self thread must not have a current context, the provided context
 * must not be current to no thread
 */
static void self_attach_ctx(Display *dpy, struct ctx_t *ctx)
{
	struct thd_data_t *self_data;
	pthread_t self;

	self = pthread_self();
	self_data = self_data_get();
	if (self_data->ctx != 0) {
		LOG("ERROR:the context %p cannot be attached to the self thread 0x%x because the self thread has already a current context %p", ctx, self, self_data->ctx);
		abort();
	}
	/* self_data->ctx == 0 */
	if (ctx->current.attached) {
		LOG("ERROR:the context %p cannot be attached to the self thread 0x%x because the context is already current the thread 0x%x", ctx, self, self_data->ctx->current.thd);
		abort();
	}
	ctx->current.thd = self;
	ctx->current.dpy = dpy;
	ctx->current.attached = true;
	self_data->ctx = ctx;
}
static void ctx_free(Display *dpy, struct ctx_t *ctx)
{
	GLsizei i;

	i = 0;
	loop {
		if (i == ctx->texs.n)
			break;
		free(ctx->texs.a[i].pixels);
		++i;
	}	
	free(ctx->texs.a);
	free(ctx);
}
/*
 * XXX: a thread cannot be cancelled while holding the global mutex, namely this
 * destructor cannot be called while holding this very thread is holding the
 * global mutex
 * XXX: thd_data is guaranted to be non 0
*/
static void thd_data_cleanup(void *key_value)
{
	struct thd_data_t *self_data;
	pthread_t self;

	self = pthread_self();
	self_data = key_value;	
	LOCK;
	if (self_data->ctx == 0)
		TRACE("the cancelled thread 0x%x has no current ctx", self);
	else {
		LOG("WARNING:the cancelled thread 0x%x has a current context %p, trying to free using the current display stored 0x%x", self, self_data->ctx, self_data->ctx->current.dpy);
		/* XXX: will crash if display is gone... may have to leak */
		ctx_free(self_data->ctx->current.dpy, self_data->ctx);
	}
	UNLOCK;
}
static GLfloat min_of_quad_plane_xs(struct ctx_t *ctx)
{
	GLfloat x;
	x = ctx->begin_end_blk.quad.vs[0].plane.x;
	if (ctx->begin_end_blk.quad.vs[1].plane.x < x)
		x = ctx->begin_end_blk.quad.vs[1].plane.x;
	if (ctx->begin_end_blk.quad.vs[2].plane.x < x)
		x = ctx->begin_end_blk.quad.vs[2].plane.x;
	if (ctx->begin_end_blk.quad.vs[3].plane.x < x)
		x = ctx->begin_end_blk.quad.vs[3].plane.x;
	return x;
}
static GLfloat min_of_quad_plane_ys(struct ctx_t *ctx)
{
	GLfloat y;
	y = ctx->begin_end_blk.quad.vs[0].plane.y;
	if (ctx->begin_end_blk.quad.vs[1].plane.y < y)
		y = ctx->begin_end_blk.quad.vs[1].plane.y;
	if (ctx->begin_end_blk.quad.vs[2].plane.y < y)
		y = ctx->begin_end_blk.quad.vs[2].plane.x;
	if (ctx->begin_end_blk.quad.vs[3].plane.y < y)
		y = ctx->begin_end_blk.quad.vs[3].plane.y;
	return y;
}
static GLfloat max_of_quad_plane_xs(struct ctx_t *ctx)
{
	GLfloat x;
	x = ctx->begin_end_blk.quad.vs[0].plane.x;
	if (ctx->begin_end_blk.quad.vs[1].plane.x > x)
		x = ctx->begin_end_blk.quad.vs[1].plane.x;
	if (ctx->begin_end_blk.quad.vs[2].plane.x > x)
		x = ctx->begin_end_blk.quad.vs[2].plane.x;
	if (ctx->begin_end_blk.quad.vs[3].plane.x > x)
		x = ctx->begin_end_blk.quad.vs[3].plane.x;
	return x;
}
static GLfloat max_of_quad_plane_ys(struct ctx_t *ctx)
{
	GLfloat y;
	y = ctx->begin_end_blk.quad.vs[0].plane.y;
	if (ctx->begin_end_blk.quad.vs[1].plane.y > y)
		y = ctx->begin_end_blk.quad.vs[1].plane.y;
	if (ctx->begin_end_blk.quad.vs[2].plane.y > y)
		y = ctx->begin_end_blk.quad.vs[2].plane.y;
	if (ctx->begin_end_blk.quad.vs[3].plane.y > y)
		y = ctx->begin_end_blk.quad.vs[3].plane.y;
	return y;
}
static GLfloat min_of_quad_tex_ss(struct ctx_t *ctx)
{
	GLfloat s;
	s = ctx->begin_end_blk.quad.vs[0].tex.s;
	if (ctx->begin_end_blk.quad.vs[1].tex.s < s)
		s = ctx->begin_end_blk.quad.vs[1].tex.s;
	if (ctx->begin_end_blk.quad.vs[2].tex.s < s)
		s = ctx->begin_end_blk.quad.vs[2].tex.s;
	if (ctx->begin_end_blk.quad.vs[3].tex.s < s)
		s = ctx->begin_end_blk.quad.vs[3].tex.s;
	return s;
}
static GLfloat min_of_quad_tex_ts(struct ctx_t *ctx)
{
	GLfloat t;
	t = ctx->begin_end_blk.quad.vs[0].tex.t;
	if (ctx->begin_end_blk.quad.vs[1].tex.t < t)
		t = ctx->begin_end_blk.quad.vs[1].tex.t;
	if (ctx->begin_end_blk.quad.vs[2].tex.t < t)
		t = ctx->begin_end_blk.quad.vs[2].tex.t;
	if (ctx->begin_end_blk.quad.vs[3].tex.t < t)
		t = ctx->begin_end_blk.quad.vs[3].tex.t;
	return t;
}
static GLfloat max_of_quad_tex_ss(struct ctx_t *ctx)
{
	GLfloat s;
	s = ctx->begin_end_blk.quad.vs[0].tex.s;
	if (ctx->begin_end_blk.quad.vs[1].tex.s > s)
		s = ctx->begin_end_blk.quad.vs[1].tex.s;
	if (ctx->begin_end_blk.quad.vs[2].tex.s > s)
		s = ctx->begin_end_blk.quad.vs[2].tex.s;
	if (ctx->begin_end_blk.quad.vs[3].tex.s > s)
		s = ctx->begin_end_blk.quad.vs[3].tex.s;
	return s;
}
static GLfloat max_of_quad_tex_ts(struct ctx_t *ctx)
{
	GLfloat t;
	t = ctx->begin_end_blk.quad.vs[0].tex.t;
	if (ctx->begin_end_blk.quad.vs[1].tex.t > t)
		t = ctx->begin_end_blk.quad.vs[1].tex.t;
	if (ctx->begin_end_blk.quad.vs[2].tex.t > t)
		t = ctx->begin_end_blk.quad.vs[2].tex.t;
	if (ctx->begin_end_blk.quad.vs[3].tex.t > t)
		t = ctx->begin_end_blk.quad.vs[3].tex.t;
	return t;
}
static GLfloat max_of_quad_alpha(struct ctx_t *ctx)
{
	GLfloat alpha;
	alpha = ctx->begin_end_blk.quad.vs[0].color.alpha;
	if (ctx->begin_end_blk.quad.vs[1].color.alpha > alpha)
		alpha = ctx->begin_end_blk.quad.vs[1].color.alpha;
	if (ctx->begin_end_blk.quad.vs[2].color.alpha > alpha)
		alpha = ctx->begin_end_blk.quad.vs[2].color.alpha;
	if (ctx->begin_end_blk.quad.vs[3].color.alpha > alpha)
		alpha = ctx->begin_end_blk.quad.vs[3].color.alpha;
	return alpha;
}
static GLfloat min_of_quad_alpha(struct ctx_t *ctx)
{
	GLfloat alpha;
	alpha = ctx->begin_end_blk.quad.vs[0].color.alpha;
	if (ctx->begin_end_blk.quad.vs[1].color.alpha < alpha)
		alpha = ctx->begin_end_blk.quad.vs[1].color.alpha;
	if (ctx->begin_end_blk.quad.vs[2].color.alpha < alpha)
		alpha = ctx->begin_end_blk.quad.vs[2].color.alpha;
	if (ctx->begin_end_blk.quad.vs[3].color.alpha < alpha)
		alpha = ctx->begin_end_blk.quad.vs[3].color.alpha;
	return alpha;
}
static void ctx_viewport_set_to_drawable_sz(Display *dpy, struct ctx_t *ctx)
{
	Status r;
	XWindowAttributes attrs;

	memset(&attrs, 0, sizeof(attrs));	
	r = XGetWindowAttributes(dpy, ctx->drawable, &attrs);
	if (r == 0) {
		LOG("%p:ERROR:unable to get the attributes of drawable 0x%x from display %p", ctx, ctx->drawable, dpy);
		ctx->vp.x = 0;
		ctx->vp.y = 0;
		ctx->vp.width = 0;
		ctx->vp.height = 0;
	} else {
		ctx->vp.x = 0;
		ctx->vp.y = 0;
		ctx->vp.width = attrs.width;
		ctx->vp.height = attrs.height;
	}
	TRACE("%p:x=%d:y=%d:width=%d:height=%d", ctx, ctx->vp.x, ctx->vp.y, ctx->vp.width, ctx->vp.height);
}
/*
 * The no_cpy flag is to notify the buffer will be fully redrawn and it is
 * useless to perform an _image_ copy, for instance if we come from a clear
 * buffer command.
 * If we fail to adjust the shm, nothing was changed.
 * There must be a shm attached to the drawable.
 */
static bool drawable_shm_adjust(struct drawable_t *d, int new_bytes_per_line,
	int new_height, int new_width, int new_depth, Visual *new_visual,
						int new_screen, bool no_cpy)
{
	int new_shm_id;
	u8 *new_shm_m;

	/* XXX: we presume the screen and visual did not change */
	TRACE("drawable 0x%x adjust:bytes_per_line=%d->%d:height=%d->%d:width=%d->%d:depth=%d->%d:no_copy=%s", d->id, d->bytes_per_line, new_bytes_per_line, d->height, new_height, d->width, new_width, d->depth, new_depth, no_cpy ? "true" : "false");
	if (no_cpy) {
		if ((d->bytes_per_line * d->height)
					> (new_bytes_per_line * new_height)) {
			TRACE("no_cpy:no reallocation of ipc shared memory segment");
			goto meta_update;
		}
		TRACE("no_cpy:reallocation of ipc shared memory segment because the buffer size did change: old buffer size=%d/new buffer size=%d", d->bytes_per_line * d->height, new_bytes_per_line * new_height);
	} else {
		/* pixel format still presumed the same, though */
		if (d->bytes_per_line == new_bytes_per_line
			&& d->height == new_height && d->width == new_width) {
			TRACE("cpy:no change in buffer format");
			goto meta_update;
		}
		TRACE("cpy:reallocation of ipc shared memory segment because the buffer format did change");
	}
	TRACE("allocation of a new ipc shared memory segment");
	new_shm_id = shmget(IPC_PRIVATE, new_bytes_per_line * new_height,
								IPC_CREAT|0777);
	if (new_shm_id == -1) {
		LOG("ERROR:unable to create a new ipc shared memory segment, keeping the old buffer");
		return false;
	}
	new_shm_m = shmat(new_shm_id, 0, 0);
	if (new_shm_m == 0) {
		shmctl(new_shm_id, IPC_RMID, 0);
		LOG("ERROR:unable to attach the new ipc shared memory segment to the process, keeping the old buffer");
		return false;
	}
	ipc_track(new_shm_id, new_shm_m);

	if (!no_cpy) {
		int min_height;
		int min_width;
#ifdef AVX2_32BITS
		struct {
			void *dst;
			u32 dst_x_offset;
			u32 dst_y_offset;
			u32 dst_width_pixs_n;
			u32 dst_line_pixs_n;
			void *src;
			u32 src_line_pixs_n;
			u32 height_lines_n;
		} avx2_input;
#else
		int x;
		int y;
#endif

		/* construct the intersection of the new and old rectangles */
		if (d->height > new_height)
			min_height = new_height;
		else	
			min_height = d->height;
		if (d->width > new_width)
			min_width = new_width;
		else
			min_width = d->width;
		TRACE("copying the intersection h/w=%d/%d\n", min_height, min_width);
#ifdef AVX2_32BITS
		avx2_input.dst = (void*)new_shm_m;
		avx2_input.dst_x_offset = 0;
		avx2_input.dst_y_offset = 0;
		avx2_input.dst_width_pixs_n = min_width;
		avx2_input.dst_line_pixs_n = new_bytes_per_line >> 2;
		avx2_input.src = (void*)d->shm.m;
		avx2_input.src_line_pixs_n = d->bytes_per_line >> 2;
		avx2_input.height_lines_n = min_height;
		glTexXImage2D_avx2(&avx2_input);
#else
		y = 0;
		loop {
			if (y == min_height)
				break;
			x = 0;
			loop {
				u32 *old_pix;
				u32 *new_pix;

				if (x == min_width)
					break;
				old_pix = (u32*)(d->shm.m + y
						* d->bytes_per_line + (x << 2));
				new_pix = (u32*)(new_shm_m + y
					* new_bytes_per_line + (x << 2));
				new_pix[0] = old_pix[0];
				++x;
			}
			++y;
		}
#endif
	}
	TRACE("getting rid of the previous ipc shared memory segment");
	shmdt(d->shm.m);
	shmctl(d->shm.id, IPC_RMID, 0);
	ipc_untrack(d->shm.id);
	d->shm.m = new_shm_m;
	d->shm.id = new_shm_id;
meta_update:	
	d->bytes_per_line = new_bytes_per_line;
	d->height = new_height;
	d->width = new_width;
	d->depth = new_depth;
	d->visual = new_visual;
	d->screen = new_screen;
	return true;
}
static struct drawable_t *get_synced_drawable_noxlock(Display *dpy, int id,
								bool no_cpy)
{
	struct drawable_t *d;
	Status r;
	XWindowAttributes attrs;
	XImage *img;
	XShmSegmentInfo info;

	d = 0;
	
	memset(&attrs, 0, sizeof(attrs));
	r = XGetWindowAttributes(dpy, id, &attrs);
	if (r == 0) {
		LOG("ERROR:unable to get the attributes of drawable 0x%x from display %p", id, dpy);
		goto exit;
	}
	TRACE("drawable=0x%x:dpy=%p:x=%d:y=%d:width=%u:height=%u:depth=%u", id, dpy, attrs.x, attrs.y, attrs.width, attrs.height, attrs.depth);
	img = XShmCreateImage(dpy, attrs.visual, attrs.depth, ZPixmap, 0, &info,
						attrs.width, attrs.height);
	if (img == 0) {
		LOG("ERROR:unable to get the XSHM image bytes per line for drawable 0x%x from display %p", id, dpy);
		goto exit;
	}
	d = ds_get(id);
	if (d == 0) { /* brand new one */
		d = ds_new(dpy, id, img->bytes_per_line, attrs.height, attrs.width,
					attrs.depth, attrs.visual,
					XScreenNumberOfScreen(attrs.screen));
		if (d == 0) {
			LOG("ERROR:unable to create an ipc shared memory segment for the drawable 0x%x from display", id, dpy);
			goto destroy_shm_img;
		}
		TRACE("new drawable 0x%u, bytes_per_line=%d, height=%d, width=%d", d->id, d->bytes_per_line, d->height, d->width);
	} else if (d->shm.id != -1) {/* XXX: can be expensive */
		bool shm_adjust_ok;

		shm_adjust_ok = drawable_shm_adjust(d, img->bytes_per_line,
			attrs.height, attrs.width, attrs.depth, attrs.visual,
				XScreenNumberOfScreen(attrs.screen), no_cpy);
		if (!shm_adjust_ok)
			d = 0;
	} else { /* d->shm.id == -1 */
		TRACE("drawable 0x%x without an ipc shared memory segment, attaching a new one", id);
		d->shm.id = shmget(IPC_PRIVATE, d->bytes_per_line * d->height,
								IPC_CREAT|0777);
		if (d->shm.id == -1) {
			LOG("ERROR:unable to create a new ipc shared memory segment for drawable 0x%x", id);
			d = 0;
		}
		d->shm.m = shmat(d->shm.id, 0, 0);
		if (d->shm.m == 0) {
			shmctl(d->shm.id, IPC_RMID, 0);
			d->shm.id = -1;
			LOG("ERROR:unable to attached memory to ipc shared memory segment id=%d for drawable 0x%x", d->shm.id, id);
			d = 0;
		}
		ipc_track(d->shm.id, d->shm.m);
	}
destroy_shm_img:
	XDestroyImage(img);
exit:
	return d;
}
static struct drawable_t *get_synced_drawable(Display *dpy, int id, bool no_cpy) 
{
	struct drawable_t *d;

	XLockDisplay(dpy);
	d = get_synced_drawable_noxlock(dpy, id, no_cpy);
	XUnlockDisplay(dpy);
	return d;
}
#define DONT_COPY true
static void color_buffer_clear(struct ctx_t *ctx)
{
	struct drawable_t *d;
#ifdef AVX2_32BITS
	struct {
		void *dst;
		u32 dst_width_pixs_n;
		u32 dst_line_bytes_n;
		u32 dst_height_lines_n;
	} avx2_input;
	TRACE("%p:clearing the color buffer:b=0x%02x:g=0x%02x:r=0x%02x:a=0x%02x", ctx, 0x25, 0x25, 0x25, 0xff);
#else
	u8 b;
	u8 g;
	u8 r;
	u8 a;
	unsigned int x;
	unsigned int y;

	/* XXX: we hardcode a reasonable clear color */
	/* b = (u8)(255. * ctx->clear_color.blue);
	 * g = (u8)(255. * ctx->clear_color.green);
	 * r = (u8)(255. * ctx->clear_color.red);
	 * a = (u8)(255. * ctx->clear_color.alpha);
	 */
	b = 0x25;
	g = 0x25;
	r = 0x25;
	a = 0x25;
	TRACE("%p:clearing the color buffer:b=0x%02x:g=0x%02x:r=0x%02x:a=0x%02x", ctx, b, g, r, a);
#endif
	d = get_synced_drawable(ctx->current.dpy, ctx->drawable, DONT_COPY); 
	if (d == 0) {
		LOG("%p:ERROR:cannot clear drawable 0x%x from display %p", ctx, ctx->drawable, ctx->current.dpy);
		return;
	}
#ifdef AVX2_32BITS
	avx2_input.dst = d->shm.m;
	avx2_input.dst_width_pixs_n = d->width;
	avx2_input.dst_line_bytes_n = d->bytes_per_line;
	avx2_input.dst_height_lines_n = d->height;
	clearcolor_avx2(&avx2_input);
#else
	y = 0;
	loop {
		if (y == d->height)
			break;
		x = 0;
		loop {
			u8 *ppixel;

			if (x == d->width)
				break;
			ppixel = d->shm.m + y * d->bytes_per_line + (x << 2);
			/* BGRA */
			ppixel[0] = b;
			ppixel[1] = g;
			ppixel[2] = r;
			ppixel[3] = a;
			++x;
		}
		++y;
	}
#endif
}
#undef DONT_COPY
/*
 * The smooth-interpolated vertex color from the fragment, including the alpha
 * channel is multiplied by the texture values. Alpha not being 1 is very
 * important. The resulting values from right above are then classically
 * blended in the framebuffer.
 */
/*
 * This is _not_ texture sampling with framebuffer pixel blending:
 *  - for a bgra src tex, this is only a rect cpy.
 *  - for a rgba src tex, this is only a rect pixel blending. 
 */
#ifdef AVX2_32BITS
#define DST_START_X	minmax_ctx_avx2.vsi[0]
#define DST_START_Y	minmax_ctx_avx2.vsi[1]
#define DST_END_X	minmax_ctx_avx2.vsi[4]
#define DST_END_Y	minmax_ctx_avx2.vsi[5]
#define SRC_START_X	minmax_ctx_avx2.vsi[2]
#define SRC_START_Y	minmax_ctx_avx2.vsi[3]
#define SRC_END_X	minmax_ctx_avx2.vsi[6]
#define SRC_END_Y	minmax_ctx_avx2.vsi[7]
static void render_in_shm(struct ctx_t *ctx)
{
	GLuint texture2d;
	struct tex_t *tex;
	struct drawable_t *d;
	struct {
		union {/* 4 xmm regs */
			GLfloat vsf[4 * 4]; /* in */
			s32	vsi[4 * 4]; /* out */
		};
		u32 scale[1 * 4];	/* in, 1 xmm reg */ 
	} minmax_ctx_avx2; /* should be packed already */
	s32 dst_y;

	texture2d = ctx->texs.texture2d;
	if (texture2d == 0) /* default texture, let's make it empty */
		return;
	tex = &ctx->texs.a[texture2d - 1];
	/*
	 * heuristic: 1 pixel texture = transparent/solid gradient using vertex
	 * color interpolation.We
	 * The 1 pixel texture is usually white and the texture GL_MODULATE
	 * will multiply the interpolated vertex color/alpha by this solid white
	 * color actually passing verbatim the interpolated colors/alpha.
	 */
	if (tex->width == 1 && tex->height == 1) {
		//TRACE("gradient using vertex color/alpha interpolation detected, skipping rendering");
		TRACE("gradient using vertex color/alpha interpolation detected, skipping rendering");
		return;
	}

	d = get_synced_drawable(ctx->current.dpy, ctx->drawable, false); 
	if (d == 0) {
		LOG("%p:ERROR:cannot render drawable 0x%x from display %p", ctx, ctx->drawable, ctx->current.dpy);
		return;
	}
	/* minmax-es of quad vs attrs */
	minmax_ctx_avx2.vsf[0]	= ctx->begin_end_blk.quad.vs[0].plane.x;
	minmax_ctx_avx2.vsf[1]	= ctx->begin_end_blk.quad.vs[0].plane.y;
	minmax_ctx_avx2.vsf[2]	= ctx->begin_end_blk.quad.vs[0].tex.s;
	minmax_ctx_avx2.vsf[3]	= ctx->begin_end_blk.quad.vs[0].tex.t;

	minmax_ctx_avx2.vsf[4]	= ctx->begin_end_blk.quad.vs[1].plane.x;
	minmax_ctx_avx2.vsf[5]	= ctx->begin_end_blk.quad.vs[1].plane.y;
	minmax_ctx_avx2.vsf[6]	= ctx->begin_end_blk.quad.vs[1].tex.s;
	minmax_ctx_avx2.vsf[7]	= ctx->begin_end_blk.quad.vs[1].tex.t;

	minmax_ctx_avx2.vsf[8]	= ctx->begin_end_blk.quad.vs[2].plane.x;
	minmax_ctx_avx2.vsf[9]	= ctx->begin_end_blk.quad.vs[2].plane.y;
	minmax_ctx_avx2.vsf[10]	= ctx->begin_end_blk.quad.vs[2].tex.s;
	minmax_ctx_avx2.vsf[11]	= ctx->begin_end_blk.quad.vs[2].tex.t;

	minmax_ctx_avx2.vsf[12]	= ctx->begin_end_blk.quad.vs[3].plane.x;
	minmax_ctx_avx2.vsf[13]	= ctx->begin_end_blk.quad.vs[3].plane.y;
	minmax_ctx_avx2.vsf[14]	= ctx->begin_end_blk.quad.vs[3].tex.s;
	minmax_ctx_avx2.vsf[15]	= ctx->begin_end_blk.quad.vs[3].tex.t;

	minmax_ctx_avx2.scale[0]	= 1;
	minmax_ctx_avx2.scale[1]	= 1;
	minmax_ctx_avx2.scale[2]	= tex->width;
	minmax_ctx_avx2.scale[3]	= tex->height;
	minmax_avx2(&minmax_ctx_avx2);
	/* ----- */

	/* in opengl, x=0/y=0 is the bottom left of the texture */
	TRACE("AVX2RENDER:ctx=%p:drawable=0x%x:tex_width=%u:tex_height=%u", ctx, ctx->drawable, tex->width, tex->height);
	TRACE("AVX2RENDER:ctx=%p:drawable=0x%x:dst_start_x=%u dst_end_x=%u src_start_x=%u src_end_x=%u", ctx, ctx->drawable, DST_START_X, DST_END_X, SRC_START_X, SRC_END_X);
	TRACE("AVX2RENDER:ctx=%p:drawable=0x%x:dst_start_y=%u dst_end_y=%u src_start_y=%u src_end_y=%u", ctx, ctx->drawable, DST_START_Y, DST_END_Y, SRC_START_Y, SRC_END_Y);
	/*--------------------------------------------------------------------*/
	/* we need to clamp to the drawable, just in case */
	if (d->height <= DST_START_Y ||  d->width <= DST_START_X)
		return;
	if (d->height < DST_END_Y) {
		DST_END_Y = d->height;
		TRACE("AVX2RENDER:clamping dst_end_y to %u due to drawable size", DST_END_Y);
	}
	if (d->width < DST_END_X) {
		DST_END_X = d->width;
		TRACE("AVX2RENDER:clamping dst_end_x to %u due to drawable size", DST_END_X);
	}
	/*--------------------------------------------------------------------*/
	/*
	 * We don't do texture sampling, just 1 to 1 pixel blending. Then we
	 * adjust the SRC_ENDs.
	 */
	SRC_END_X = SRC_START_X + DST_END_X - DST_START_X;
	SRC_END_Y = SRC_START_Y + DST_END_Y - DST_START_Y;
	/*--------------------------------------------------------------------*/
	/* we need to clamp to the texture, just in case */
	if (tex->height <= SRC_START_Y || tex->width <= SRC_START_X) 
		return;
	if (tex->height < SRC_END_Y) {
		DST_END_Y = DST_START_Y + tex->height;
		TRACE("AVX2RENDER:clamping dst_end_y to %u due to texture size", DST_END_Y);
	}
	if (tex->width < SRC_END_X) {
		DST_END_X  = DST_START_X + tex->width;
		TRACE("AVX2RENDER:clamping dst_end_x to %u due to texture size", DST_END_X);
	}
	/*--------------------------------------------------------------------*/
	if (tex->format == GL_BGRA) {
		struct {
			u8 *dst;
			u32 dst_x_offset;
			u32 dst_y_offset;
			u32 dst_width_pixs_n;
			u32 dst_line_pixs_n;
			u8 *src;
			u32 src_line_pixs_n;
			u32 height_lines_n;
		} avx2_input;

		avx2_input.dst = d->shm.m;
		avx2_input.dst_x_offset = DST_START_X;
		avx2_input.dst_y_offset = DST_START_Y;
		avx2_input.dst_width_pixs_n = DST_END_X - DST_START_X;
		avx2_input.dst_line_pixs_n = d->bytes_per_line >> 2;
		avx2_input.src = tex->pixels + 
				((SRC_START_Y * tex->width + SRC_START_X) << 2);
		avx2_input.src_line_pixs_n =  tex->width;
		avx2_input.height_lines_n = DST_END_Y - DST_START_Y;
		glTexXImage2D_avx2(&avx2_input);
					
	} else { /* tex->format == GL_RGBA */
		struct {
			u8 *dst;
			u32 dst_adjust_bytes_n;
			u8 *src;
			u32 src_adjust_bytes_n;
			u32 width_pixs_n;
			u32 height_lines_n;
		} alphablend_rgba_input_avx2;

		/* the width and heiht driver values are DST_a_[XY] */
		alphablend_rgba_input_avx2.dst = d->shm.m + DST_START_Y
				* d->bytes_per_line + (DST_START_X << 2);

		alphablend_rgba_input_avx2.dst_adjust_bytes_n =
			d->bytes_per_line - ((DST_END_X - DST_START_X) << 2);

		alphablend_rgba_input_avx2.src = tex->pixels +
			((SRC_START_Y * tex->width + SRC_START_X) << 2);

		alphablend_rgba_input_avx2.src_adjust_bytes_n =
				(tex->width - (DST_END_X - DST_START_X)) << 2;
			
		alphablend_rgba_input_avx2.width_pixs_n =
							DST_END_X - DST_START_X;
		alphablend_rgba_input_avx2.height_lines_n =
							DST_END_Y - DST_START_Y;
		alphablend_rgba_avx2(&alphablend_rgba_input_avx2);
	}
}
#else
static void render_in_shm(struct ctx_t *ctx)
{
	GLuint texture2d;
	struct tex_t *tex;
	GLfloat src_primary_alpha_min;
	GLfloat src_primary_alpha_max;
	struct drawable_t *d;
	int dst_shm_width;
	int dst_shm_height;	
	int dst_shm_bytes_per_line;
	size_t dst_shm_sz;
	u8 *dst;
	GLfloat dst_w;
	GLfloat dst_h;
	GLfloat dst_line_bytes_n;
	u8 *src;
	GLfloat src_w;
	GLfloat src_h;
	GLenum src_f;
	GLfloat dst_start_x;
	GLfloat dst_start_y;
	GLfloat dst_end_x;
	GLfloat dst_end_y;
	GLfloat src_start_s;
	GLfloat src_start_t;
	GLfloat src_end_s;
	GLfloat src_end_t;
	GLfloat src_start_x;
	GLfloat src_start_y;
	GLfloat src_end_x;
	GLfloat src_end_y;
	GLfloat dst_y;

	texture2d = ctx->texs.texture2d;
	if (texture2d == 0) /* default texture, let's make it empty */
		return;
	/*--------------------------------------------------------------------*/
	src_primary_alpha_min = min_of_quad_alpha(ctx);
	src_primary_alpha_max = max_of_quad_alpha(ctx);
	TRACE("alpha_min=%f alpha_max=%f", src_primary_alpha_min, src_primary_alpha_max);
	
	/*--------------------------------------------------------------------*/
	d = get_synced_drawable(ctx->current.dpy, ctx->drawable, false); 
	if (d == 0) {
		LOG("%p:ERROR:cannot render drawable 0x%x from display %p", ctx, ctx->drawable, ctx->current.dpy);
		return;
	}
	/*--------------------------------------------------------------------*/
	tex = &ctx->texs.a[texture2d - 1];
	src = tex->pixels;
	src_f = tex->format;
	src_w = tex->width;
	src_h = tex->height;
	/*
	 * The following actually gives use the quad orientation.
	 * Those are 3D vertex coords, _not_ pixel indexes.
	 * "end" is supposed to be the index of the pixel past the last
	 * fragment pixel.
	 */
	dst_start_x = min_of_quad_plane_xs(ctx);
	dst_start_y = min_of_quad_plane_ys(ctx);
	dst_end_x = max_of_quad_plane_xs(ctx);
	dst_end_y = max_of_quad_plane_ys(ctx);

	src_start_s = min_of_quad_tex_ss(ctx);
	src_start_t = min_of_quad_tex_ts(ctx);
	src_end_s = max_of_quad_tex_ss(ctx);
	src_end_t = max_of_quad_tex_ts(ctx);

	src_start_x = src_start_s * (GLfloat)src_w;
	src_start_y = src_start_t * (GLfloat)src_h;
	src_end_x = src_end_s * (GLfloat)src_w;
	src_end_y = src_end_t * (GLfloat)src_h;
	/* in opengl, x=0/y=0 is the bottom left of the texture */
	TRACE("ctx=%p:drawable=0x%x:tex_width=%u:tex_height=%u", ctx, ctx->drawable, tex->width, tex->height);
	TRACE("ctx=%p:drawable=0x%x:dst_start_x=%f dst_end_x=%f src_start_x=%f src_end_x=%f", ctx, ctx->drawable, dst_start_x, dst_end_x, src_start_x, src_end_x);
	TRACE("ctx=%p:drawable=0x%x:dst_start_y=%f dst_end_y=%f src_start_y=%f src_end_y=%f", ctx, ctx->drawable, dst_start_y, dst_end_y, src_start_y, src_end_y);
	/*--------------------------------------------------------------------*/
	dst = d->shm.m;
	dst_w = (GLfloat)d->width;
	dst_h = (GLfloat)d->height;
	dst_line_bytes_n = (GLfloat)d->bytes_per_line;
	/*--------------------------------------------------------------------*/
	/*
	 * we need to clamp to the drawable, just in case, they are supposed to
	 * be perfectly matched due to the orthogonal projection.
	 */
	if (dst_h <= dst_start_y ||  dst_w <= dst_start_x)
		return;
	if (dst_h < dst_end_y) {
		dst_end_y = dst_h;
		TRACE("clamping dst_end_y to %f", dst_end_y);
	}
	if (dst_w < dst_end_x) {
		dst_end_x = dst_w;
		TRACE("clamping dst_end_x to %f", dst_end_x);
	}
	/*--------------------------------------------------------------------*/
	/*
	 * heuristic: 1 pixel texture = transparent/solid gradient using vertex
	 * color interpolation.We
	 * The 1 pixel texture is usually white and the texture GL_MODULATE
	 * will multiply the interpolated vertex color/alpha by this solid white
	 * color actually passing verbatim the interpolated colors/alpha.
	 */
	if (src_w == 1 && src_h == 1) {
		TRACE("gradient using vertex color/alpha interpolation detected, skipping rendering");
		return;
	}
	dst_y = dst_start_y;
	loop {
		GLfloat dst_x;
		if (dst_y >= dst_end_y)
			break;

		dst_x = dst_start_x;
		loop {
			GLfloat src_x;
			GLfloat src_y;

			if (dst_x >= dst_end_x)
				break;
			src_x = src_start_x + dst_x - dst_start_x;
			src_y = src_start_y + dst_y - dst_start_y;
			if (src_x < src_end_x && src_y < src_end_y) {
				u8 *dst_pix;
				u8 *src_pix;

				dst_pix = dst + (int)(dst_y * dst_line_bytes_n
								+ dst_x * 4);
				src_pix = src + (int)((src_y * src_w + src_x)
									* 4);
				/* XXX: dst_f is presumed BGRA */
				if (src_f == GL_RGBA) {
					GLfloat dst_color;
					GLfloat src_color;
					GLfloat src_alpha;

					if (src_pix[3] != 255) {
						src_alpha = src_pix[3];
						src_alpha /= 255.;

						dst_color = dst_pix[0];
						src_color = src_pix[2];
						dst_color = dst_color * (1. - src_alpha) + src_color * src_alpha;
						if (dst_color > 255.)
							dst_color = 255.;
						dst_pix[0] = (u8)dst_color;

						dst_color = dst_pix[1];
						src_color = src_pix[1];
						dst_color = dst_color * (1. - src_alpha) + src_color * src_alpha;
						if (dst_color > 255.)
							dst_color = 255.;
						dst_pix[1] = (u8)dst_color;

						dst_color = dst_pix[2];
						src_color = src_pix[0];
						dst_color = dst_color * (1. - src_alpha) + src_color * src_alpha;
						if (dst_color > 255.)
							dst_color = 255.;
						dst_pix[2] = (u8)dst_color;

						dst_pix[3] = src_pix[3];
					} else {
						dst_pix[0]=src_pix[2];
						dst_pix[1]=src_pix[1];
						dst_pix[2]=src_pix[0];
						dst_pix[3]=src_pix[3];
					}
				} else if (src_f == GL_BGRA) {
					GLfloat dst_color;
					GLfloat src_color;
					GLfloat src_alpha;

					if (src_pix[3] != 255) {
						src_alpha = src_pix[3];
						src_alpha /= 255.;

						dst_color = dst_pix[0];
						src_color = src_pix[0];
						dst_color = dst_color * (1. - src_alpha) + src_color * src_alpha;
						if (dst_color > 255.)
							dst_color = 255.;
						dst_pix[0] = (u8)dst_color;

						dst_color = dst_pix[1];
						src_color = src_pix[1];
						dst_color = dst_color * (1. - src_alpha) + src_color * src_alpha;
						if (dst_color > 255.)
							dst_color = 255.;
						dst_pix[1] = (u8)dst_color;

						dst_color = dst_pix[2];
						src_color = src_pix[2];
						dst_color = dst_color * (1. - src_alpha) + src_color * src_alpha;
						if (dst_color > 255.)
							dst_color = 255.;
						dst_pix[2] = (u8)dst_color;
					
						dst_pix[3] = src_pix[3];
					} else {
						dst_pix[0]=src_pix[0];
						dst_pix[1]=src_pix[1];
						dst_pix[2]=src_pix[2];
						dst_pix[3]=src_pix[3];
					}
				}
			}
			++dst_x;
		}
		++dst_y;
	}
}
#endif
/*}}}*/
#include "gl.c"
#include "glx.c"
